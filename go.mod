module gitlab.com/luberryscc/led-spy-designer

go 1.15

require (
	github.com/davecgh/go-spew v1.1.1
	github.com/deepakjois/gousbdrivedetector v0.0.0-20161027045320-4d29e4d6f1b7
	github.com/gomodule/redigo v1.8.5 // indirect
	github.com/google/uuid v1.2.0
	github.com/googollee/go-socket.io v1.6.1
	github.com/guark/guark v0.1.1
	github.com/guark/plugins v0.0.0-20210821185706-126e378321e2
	github.com/json-iterator/go v1.1.12
	github.com/melbahja/bundler v0.0.0-20201106222256-0b0123a34616
	github.com/mikepb/go-serial v0.0.0-20201030162908-19fa9bf168fc
	github.com/patrickmn/go-cache v2.1.0+incompatible
	github.com/pkg/errors v0.9.1
	github.com/simpleiot/simpleiot v0.0.29
	github.com/sirupsen/logrus v1.8.1
	gitlab.com/luberryscc/led-spy-protos v0.0.0-20220830163804-2b02bafe209e
	go.bug.st/serial v1.3.3
	google.golang.org/protobuf v1.26.0
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b
)

replace github.com/guark/guark => github.com/luberry/guark v0.1.2-0.20211013005859-6224e40ae9f1

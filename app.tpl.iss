[Setup]
AppName={{.Name}}
AppVersion={{.Ver}}
AppPublisher={{.Publisher}}
SetupIconFile=ui\public\favicon.ico
WizardStyle=modern
DefaultDirName={autopf}\{{.Name}}
UninstallDisplayIcon={app}\{{.ID}}.exe
DefaultGroupName={{.Name}}
Compression=lzma2
SolidCompression=yes
OutputDir={{.Dest}}
OutputBaseFilename={{.ID}}-{{.Ver}}Setup
ArchitecturesInstallIn64BitMode=x64

[Files]
Source: "{{.Dest}}\windows\{{.ID}}.exe"; DestDir: "{app}"
Source: "{{.Dest}}\windows\statics\icon.png"; DestDir: "{app}\statics"
Source: "resources\windows\bossa.inf"; DestDir: "{app}\drivers"
Source: "resources\windows\bossa.cat"; DestDir: "{app}\drivers"
Source: "{{.Dest}}\windows\webview.dll"; DestDir: "{app}"
Source: "{{.Dest}}\windows\WebView2Loader.dll"; DestDir: "{app}"

[Run] 
Filename: {sys}\pnputil.exe; WorkingDir: "{app}\drivers";Description: "install CDC driver";StatusMsg: "Installing CDC driver..."; Parameters: "/add-driver bossa.inf /install"

[UninstallRun] 
Filename: {sys}\pnputil.exe; WorkingDir: "{app}\drivers";StatusMsg: "Uninstalling CDC driver..."; Parameters: "/delete-driver bossa.inf /uninstall"

[Icons]
Name: "{group}\{{.Name}}"; Filename: "{app}\{{.ID}}.exe"
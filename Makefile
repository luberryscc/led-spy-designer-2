


windows:
	CGO_ENABLED=1 CGO_LDFLAGS="-lsetupapi"  guark build --rm --target=windows && cp resources/windows/bossa* dist/windows/

dist-windows: windows
	go run cmd/template/main.go && docker run --rm -i -v "${PWD}:/work" amake/innosetup app.iss

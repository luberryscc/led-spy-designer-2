const isProd = process.env.NODE_ENV === 'production';
const HtmlWebpackPlugin = require('html-webpack-plugin');
const { useGuarkLockFile, checkBeforeBuild } = require('guark/build');
const stylesHandler = 'style-loader';

const config = {
    mode: isProd ? 'production' : 'development',
    entry: {
        index: './src/index.tsx',
    },
    output: {
        path: process.env.GUARK_BUILD_DIR,
    },
    resolve: {
        extensions: ['.js', '.jsx', '.ts', '.tsx'],
    },
    module: {
        rules: [
            {
                test: /\.(tsx|ts)?$/,
                use: 'babel-loader',
                exclude: /node_modules/,
            },
            {
                test: /\.js$/,
                include: /node_modules\/react-dom/,
                use: ['react-hot-loader/webpack'],
            },
            {
                test: /\.(tsx|ts|js|jsx)?$/,
                enforce: 'pre',
                use: ['source-map-loader'],
            },
            {
                test: /\.css$/i,
                use: [stylesHandler, 'css-loader'],
            },
            {
                test: /\.less$/i,
                use: ['less-loader'],
            },
            {
                test: /\.(eot|svg|ttf|woff|woff2|png|jpg|gif)$/i,
                type: 'asset',
            },
            {
                test: /\.scss$/,
                use: ['style-loader', 'css-loader', 'sass-loader'],
            },
        ],
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: './src/index.html',
            filename: 'index.html',
            inject: 'body',
        }),
    ],
};

if (isProd) {
    /*  config.optimization = {
    minimizer: [new TerserWebpackPlugin()],
  };*/
} else {
    config.devServer = {
        hot: true,
        open: false,
        // After server started you should call useGuarkLockFile.
        after: (app, server, compiler) => compiler.hooks.done.tap('Guark', useGuarkLockFile),
    };
}

checkBeforeBuild();
module.exports = config;

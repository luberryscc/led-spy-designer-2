import React, { Dispatch, useEffect } from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Slide from '@material-ui/core/Slide';
import { TransitionProps } from '@material-ui/core/transitions';
import { useButtonConfig, useReactive } from '../store/config/hooks';
import { useDispatch } from 'react-redux';
import { setAllButtons, setButtonConfig } from '../store/config/actions';
import { Color, ColorPicker, createColor } from 'material-ui-color';
import {
    ButtonGroup,
    Checkbox,
    createStyles,
    FormControlLabel,
    FormGroup,
    Input,
    makeStyles,
    Theme,
} from '@material-ui/core';
import { get, noop } from 'lodash';
import { ButtonConfig } from '../store/config/types';
import { useTheme } from '@material-ui/core';

const Transition = React.forwardRef(function Transition(
    props: TransitionProps & { children?: React.ReactElement<any, any> },
    ref: React.Ref<unknown>
) {
    return <Slide direction="up" ref={ref} {...props} />;
});

interface Props {
    buttonId: number;
    open: boolean;
    buttonLabel: string;
    handleClose: () => void;
    numButtons: number;
    currentConfig?: ButtonConfig;
}

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        mapping: {
            width: '75%',
        },
        warning: {
            color: theme.palette.warning.main,
        },
    })
);
const ButtonDialog: React.FC<Props> = ({
    buttonId = NaN,
    buttonLabel = '',
    open,
    handleClose,
    numButtons,
}) => {
    const theme = useTheme();
    const classes = useStyles(theme);
    const dispatch: Dispatch<any> = useDispatch();
    const currentConfig = useButtonConfig(buttonId);
    const [color, setColor] = React.useState(
        createColor(get(currentConfig, 'color', '#000000FF').slice(0, 7))
    );
    const [rColor, setRColor] = React.useState(
        createColor(get(currentConfig, 'reactColor', '#000000FF').slice(0, 7))
    );
    const [all, setAll] = React.useState(false);
    const [disabled, setDisabled] = React.useState(currentConfig.disabled);
    const [mapping, setMapping] = React.useState(get(currentConfig, 'mapping.mappingId', 0));
    const reactive = useReactive();
    const handleSubmit = () => {
        const newConfig = currentConfig;
        if (!newConfig) return;
        color.alpha = 1;
        rColor.alpha = 1;
        newConfig.color = `#${color.hex}FF`;
        newConfig.reactColor = `#${reactive ? rColor.hex : color.hex}FF`;
        const id = buttonId;
        newConfig.mapping = {
            buttonId: id,
            mappingId: mapping,
        };

        newConfig.disabled = disabled;
        if (all) {
            dispatch(setAllButtons(id, newConfig));
            return;
        }
        dispatch(setButtonConfig(id, newConfig));
        handleClose();
    };
    const mappingDisabled = mapping == 255;
    const handleCancel = () => {
        handleClose();
    };
    const onMappingChange = (val: number) => {
        if ((val < 0 || val >= numButtons) && val != 255) return;
        setMapping(val);
    };
    const handleMappingChange = (e: any) => {
        onMappingChange(e.target.value);
    };
    const renderPicker = (value: Color, onChange: (newValue: Color) => void, label: string) => (
        <FormControlLabel
            control={
                <ColorPicker
                    doPopup={mappingDisabled ? () => false : undefined}
                    disableAlpha={true}
                    disablePlainColor={true}
                    onChange={mappingDisabled ? noop : onChange}
                    value={value}
                />
            }
            labelPlacement={'top'}
            label={label}
        />
    );
    return (
        <Dialog
            open={open}
            TransitionComponent={Transition}
            keepMounted
            onClose={handleCancel}
            aria-labelledby="alert-dialog-slide-title"
            aria-describedby="alert-dialog-slide-description"
        >
            <DialogTitle id="alert-dialog-slide-title">
                {`Set color${reactive ? 's' : ''} and mapping for the ${buttonLabel} button.`}
            </DialogTitle>
            <DialogContent>
                {mappingDisabled && (
                    <DialogContentText className={classes.warning}>
                        Note: disabling mapping will cause you to lose all color data for this
                        button.
                    </DialogContentText>
                )}
                <FormGroup row={true}>
                    {renderPicker(color, setColor, 'Base color')}
                    <FormControlLabel
                        labelPlacement={'top'}
                        control={
                            <ButtonGroup>
                                <Input
                                    className={classes.mapping}
                                    id="standard-number"
                                    value={!mappingDisabled && mapping}
                                    onChange={handleMappingChange}
                                    placeholder={'unmapped'}
                                    type="number"
                                />
                                <Button size={'small'} onClick={() => onMappingChange(255)}>
                                    UnMap
                                </Button>
                            </ButtonGroup>
                        }
                        label="Led Index"
                    />
                </FormGroup>
                {reactive && (
                    <FormGroup row={true}>
                        {renderPicker(rColor, setRColor, 'Reactive color')}
                    </FormGroup>
                )}
                <FormGroup row={true}>
                    <FormControlLabel
                        control={
                            <Checkbox
                                disabled={mappingDisabled}
                                value={all}
                                defaultChecked={all}
                                onChange={() => setAll(!all)}
                                name="Apply color to all buttons."
                            />
                        }
                        label={`Apply color${reactive ? 's' : ''} to all buttons`}
                    />
                    <FormControlLabel
                        control={
                            <Checkbox
                                disabled={all}
                                value={disabled}
                                defaultChecked={disabled}
                                onChange={(_: any, checked: boolean) => {
                                    if (checked != disabled) setDisabled(checked);

                                    setDisabled(checked);
                                }}
                                name="Disable leds for this button"
                            />
                        }
                        label={`Disable leds for this button`}
                    />
                </FormGroup>
            </DialogContent>
            <DialogActions>
                <Button onClick={handleCancel} color="primary">
                    Cancel
                </Button>
                <Button onClick={handleSubmit} color="primary">
                    Save
                </Button>
            </DialogActions>
        </Dialog>
    );
};
export default ButtonDialog;

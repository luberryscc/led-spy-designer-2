import React, { Dispatch } from 'react';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import { useDispatch } from 'react-redux';
import { drawerWidth } from './Sidebar';
import { useCurrentProfileId, useMaxProfiles, useNumProfiles } from './store/config/hooks';
import { addProfile, removeProfile, setCurrentProfileId } from './store/config/actions';
import {
    Button,
    ButtonGroup,
    Dialog,
    DialogActions,
    DialogContent,
    DialogContentText,
    DialogTitle,
    IconButton,
} from '@material-ui/core';
import DeleteIcon from '@material-ui/icons/Delete';
import AddIcon from '@material-ui/icons/Add';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        formControl: {
            margin: theme.spacing(1),
            width: drawerWidth - 40,
            display: 'flex',
            justifyContent: 'space-between',
        },
        selectEmpty: {
            marginTop: theme.spacing(2),
        },
        select: {
            width: drawerWidth - 120,
        },
        menuItem: {
            display: 'flex',
            justifyContent: 'space-between',
        },
        deleteButton: {},
    })
);

const ProfileSelect: React.FC = () => {
    const dispatch: Dispatch<any> = useDispatch();

    const profile = useCurrentProfileId();
    const classes = useStyles();
    const numProfiles = useNumProfiles();
    const [open, setOpen] = React.useState(false);
    const handleChange = (e: any) => {
        dispatch(setCurrentProfileId(e.target.value));
    };
    const maxProfiles = useMaxProfiles();

    const getItems = () => {
        const arr: number[] = [];
        for (let i = 0; i < numProfiles; i++) arr.push(i);
        return arr;
    };
    const items = getItems();
    const handleDelete = () => {
        dispatch(removeProfile(profile));
    };
    const handleClose = () => {
        setOpen(false);
    };
    const handleAdd = () => {
        dispatch(addProfile());
    };
    const [modalOpen, setModalOpen] = React.useState(false);
    const handleDialogClose = () => setModalOpen(false);
    const handleOk = () => {
        handleDelete();
        handleDialogClose();
    };

    return (
        <div>
            <Dialog
                open={modalOpen}
                onClose={handleDialogClose}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
            >
                <DialogTitle id="alert-dialog-title">
                    {`Are you sure you want to delete profile ${profile}?`}
                </DialogTitle>
                <DialogContent>
                    <DialogContentText id="alert-dialog-description">
                        This will permanantly delete this profile
                    </DialogContentText>
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleDialogClose} color="primary">
                        Cancel
                    </Button>
                    <Button onClick={handleOk} color="primary" autoFocus>
                        Confirm
                    </Button>
                </DialogActions>
            </Dialog>
            <FormControl className={classes.formControl}>
                <div>
                    <ButtonGroup>
                        <Select
                            className={classes.select}
                            labelId="demo-simple-select-helper-label"
                            id="demo-simple-select-helper"
                            value={profile}
                            open={open}
                            color="primary"
                            onChange={handleChange}
                            onOpen={() => setOpen(true)}
                            onClose={handleClose}
                        >
                            {items.map((id: number) => (
                                <MenuItem className={classes.menuItem} key={id} value={id}>
                                    {`Profile ${id}`}
                                </MenuItem>
                            ))}
                        </Select>
                        <IconButton
                            disabled={numProfiles == 1}
                            aria-label="delete-profile"
                            color="primary"
                            onClick={() => setModalOpen(true)}
                        >
                            <DeleteIcon />
                        </IconButton>
                        <IconButton
                            disabled={numProfiles >= maxProfiles}
                            aria-label="add-profile"
                            color="primary"
                            onClick={() => handleAdd()}
                        >
                            <AddIcon />
                        </IconButton>
                    </ButtonGroup>
                </div>
            </FormControl>
        </div>
    );
};

export default ProfileSelect;

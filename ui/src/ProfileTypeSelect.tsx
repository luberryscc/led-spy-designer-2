import React, { Dispatch } from 'react';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import MenuItem from '@material-ui/core/MenuItem';
import * as protos from 'led-spy-protos/config';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import { useDispatch } from 'react-redux';
import { drawerWidth } from './Sidebar';
import { useCurrentProfileId, useCurrentProfileType } from './store/config/hooks';
import { addProfile, setCurrentProfileType } from './store/config/actions';
import {
    Button,
    Dialog,
    DialogActions,
    DialogContent,
    DialogContentText,
    DialogTitle,
} from '@material-ui/core';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        formControl: {
            margin: theme.spacing(1),
            width: drawerWidth - 40,
            display: 'flex',
            justifyContent: 'space-between',
        },
        selectEmpty: {
            marginTop: theme.spacing(2),
        },
        select: {
            width: drawerWidth - 40,
        },
        menuItem: {
            display: 'flex',
            justifyContent: 'space-between',
        },
        deleteButton: {},
    })
);

const ProfileTypeSelect: React.FC = () => {
    const dispatch: Dispatch<any> = useDispatch();

    const profileType = useCurrentProfileType();
    const profile = useCurrentProfileId();
    const classes = useStyles();
    const [open, setOpen] = React.useState(false);
    const [modalOpen, setModalOpen] = React.useState(false);
    const [newType, setNewType] = React.useState(profileType);
    const handleChange = (e: any) => {
        setNewType(e.target.value);
        setModalOpen(true);
    };
    const types = ['Static', 'Reactive'];

    const handleClose = () => {
        setOpen(false);
    };
    const handleAdd = () => {
        dispatch(addProfile());
    };
    const handleDialogClose = () => setModalOpen(false);
    const handleOk = () => {
        dispatch(setCurrentProfileType(newType));
        handleDialogClose();
    };
    const handleCancel = () => {
        if (newType == profileType) return;
        setNewType(profileType);
        handleDialogClose();
    };

    return (
        <div>
            <Dialog
                open={modalOpen}
                onClose={handleDialogClose}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
            >
                <DialogTitle id="alert-dialog-title">
                    {`Are you sure you want to change the type of profile ${profile}?`}
                </DialogTitle>
                <DialogContent>
                    <DialogContentText id="alert-dialog-description">
                        You may lose some configuration data such as reactive colors.
                    </DialogContentText>
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleCancel} color="primary">
                        Cancel
                    </Button>
                    <Button onClick={handleOk} color="primary" autoFocus>
                        Confirm
                    </Button>
                </DialogActions>
            </Dialog>
            <FormControl className={classes.formControl}>
                <Select
                    className={classes.select}
                    labelId="demo-simple-select-helper-label"
                    id="demo-simple-select-helper"
                    value={`${profileType}`}
                    open={open}
                    onChange={handleChange}
                    onOpen={() => setOpen(true)}
                    onClose={handleClose}
                >
                    {types &&
                        types.map((key: string, tp: protos.Config.ProfileType) => (
                            <MenuItem key={tp} value={tp}>
                                {key}
                            </MenuItem>
                        ))}
                </Select>
            </FormControl>
        </div>
    );
};

export default ProfileTypeSelect;

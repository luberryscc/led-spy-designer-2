import { Button, ButtonGroup } from '@material-ui/core';
import React from 'react';
import { Dispatch } from 'react';
import { useDispatch } from 'react-redux';
import { useConfig, useConfigFetching, useConfigWriting } from './store/config/hooks';
import AddIcon from '@material-ui/icons/Add';
import SaveIcon from '@material-ui/icons/Save';
import { FileDialog } from './FileDialog';
import { readConfigFromFile, saveConfigToFile } from './store/config/actions';

export const SaveLoad = () => {
    const dispatch: Dispatch<any> = useDispatch();
    const isWriting = useConfigWriting();
    const isReading = useConfigFetching();
    const [file, setFile] = React.useState('');
    const [saveOpen, setSaveOpen] = React.useState(false);
    const [loadOpen, setLoadOpen] = React.useState(false);
    const accept = 'lspy';
    const cfg = useConfig();
    const handleLoadCancel = () => {
        setLoadOpen(false);
    };
    const handleLoadSubmit = (file: string) => {
        dispatch(readConfigFromFile(file));
        setLoadOpen(false);
    };
    const handleSaveCancel = () => {
        setSaveOpen(false);
    };
    const handleSaveSubmit = (file: string) => {
        if (!cfg) return;
        dispatch(saveConfigToFile(file, cfg));
        setSaveOpen(false);
    };
    const disabled = isWriting || isReading;

    return (
        <React.Fragment>
            <FileDialog
                open={loadOpen}
                onCancel={handleLoadCancel}
                onSubmit={handleLoadSubmit}
                title={'Load configuration from file.'}
                submitText={'Load'}
                accept={accept}
            />
            <FileDialog
                open={saveOpen}
                onCancel={handleSaveCancel}
                onSubmit={handleSaveSubmit}
                title={'Save configuration to file.'}
                submitText={'Save'}
                isSave
                accept={accept}
            />
            <ButtonGroup>
                <Button
                    disabled={disabled}
                    onClick={() => setLoadOpen(true)}
                    startIcon={<AddIcon />}
                >
                    Load
                </Button>
                <Button
                    onClick={() => setSaveOpen(true)}
                    disabled={disabled}
                    startIcon={<SaveIcon />}
                >
                    Save
                </Button>
            </ButtonGroup>
        </React.Fragment>
    );
};

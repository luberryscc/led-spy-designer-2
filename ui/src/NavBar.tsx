import React, { Fragment, useState } from 'react';
import { fade, makeStyles, Theme, createStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import MenuIcon from '@material-ui/icons/Menu';
import SerialPort from './SerialPort';
import { Button, ButtonGroup, FormControlLabel, Switch } from '@material-ui/core';
import {
    useClosingPort,
    useCurrentPort,
    useCurrentPortSetting,
    useFlashing,
} from './store/serial/hooks';
import { isEmpty, noop, isNull } from 'lodash';
import clsx from 'clsx';
import {
    useConfigFetching,
    useConfigWriting,
    useConfig,
    useIsInputViewer,
    useDrawerOpen,
    useControllerType,
    useControllerVariant,
    useNumButtons,
} from './store/config/hooks';
import { fetchConfig, isDrawerOpen, isInputViewer, writeConfig } from './store/config/actions';
import { useDispatch } from 'react-redux';
import { Dispatch } from 'redux';
import { drawerWidth, Sidebar } from './Sidebar';
import { flashFirmware } from './store/serial/actions';
import { FileDialog } from './FileDialog';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        appBar: {
            transition: theme.transitions.create(['margin', 'width'], {
                easing: theme.transitions.easing.sharp,
                duration: theme.transitions.duration.leavingScreen,
            }),
        },
        appBarShift: {
            width: `calc(100% - ${drawerWidth}px)`,
            marginLeft: drawerWidth,
            transition: theme.transitions.create(['margin', 'width'], {
                easing: theme.transitions.easing.easeOut,
                duration: theme.transitions.duration.enteringScreen,
            }),
        },
        menuButton: {
            marginRight: theme.spacing(2),
        },
        grow: {
            flexGrow: 1,
        },
        hide: {
            display: 'none',
        },
        title: {
            display: 'none',
            [theme.breakpoints.up('sm')]: {
                display: 'block',
            },
        },
        port: {
            position: 'relative',
            borderRadius: theme.shape.borderRadius,
            backgroundColor: fade(theme.palette.common.white, 0.15),
            '&:hover': {
                backgroundColor: fade(theme.palette.common.white, 0.25),
            },
            marginRight: 0,
            marginLeft: 0,
            width: '100%',
            [theme.breakpoints.up('sm')]: {
                marginLeft: theme.spacing(3),
                width: 'auto',
            },
        },
        searchIcon: {
            padding: theme.spacing(0, 2),
            height: '100%',
            position: 'absolute',
            pointerEvents: 'none',
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
        },
        inputRoot: {
            color: 'inherit',
        },
        inputInput: {
            padding: theme.spacing(1, 1, 1, 0),
            // vertical padding + font size from searchIcon
            paddingLeft: `calc(1em + ${theme.spacing(4)}px)`,
            transition: theme.transitions.create('width'),
            width: '100%',
            [theme.breakpoints.up('md')]: {
                width: '20ch',
            },
        },
        sectionDesktop: {
            display: 'none',
            [theme.breakpoints.up('md')]: {
                display: 'flex',
            },
        },
        sectionMobile: {
            display: 'flex',
            [theme.breakpoints.up('md')]: {
                display: 'none',
            },
        },
    })
);

const NavBar: React.FC = () => {
    const dispatch: Dispatch<any> = useDispatch();
    const classes = useStyles();
    const inputView = useIsInputViewer();

    const handleDrawerOpen = () => {
        dispatch(isDrawerOpen(true));
    };
    const isFetchingConfig = useConfigFetching();
    const isWritingConfig = useConfigWriting();
    const isSettingPort = useCurrentPortSetting();
    const isClosingPort = useClosingPort();
    const isFlashing = useFlashing();
    const isInputView = useIsInputViewer();
    const loading =
        isFetchingConfig ||
        isWritingConfig ||
        isSettingPort ||
        isClosingPort ||
        isFlashing ||
        isInputView;

    const currentConfig = useConfig();
    const controllerType = useControllerType();
    const variant = useControllerVariant();
    const numButtons = useNumButtons();

    const currentPort = useCurrentPort();
    const drawerOpen = useDrawerOpen();
    const hasCurrentPort = !isEmpty(currentPort);
    const disabled = !hasCurrentPort || loading;
    const [fwModalOpen, setFWModalOpen] = useState(false);

    const fetchCfg = () => dispatch(fetchConfig());

    const writeCfg = () => {
        const cfg = currentConfig;
        return isNull(cfg) ? noop : dispatch(writeConfig(cfg));
    };
    const fwCancel = () => {
        setFWModalOpen(false);
    };
    const fwSubmit = (file: string) => {
        dispatch(flashFirmware(file));
        setFWModalOpen(false);
    };

    const setInputViewer = (_: React.ChangeEvent<HTMLInputElement>, checked: boolean) => {
        dispatch(isInputViewer(checked, numButtons, controllerType, variant));
    };

    const menuId = 'primary-search-account-menu';

    return (
        <div className={classes.grow}>
            <FileDialog
                open={fwModalOpen}
                onCancel={fwCancel}
                onSubmit={fwSubmit}
                title={'Flash firmware file'}
                submitText={'Flash'}
                accept={'uf2'}
            />
            <AppBar
                position="fixed"
                className={clsx(classes.appBar, {
                    [classes.appBarShift]: drawerOpen,
                })}
            >
                {' '}
                <Toolbar>
                    <IconButton
                        edge="start"
                        //className={classes.menuButton}
                        onClick={handleDrawerOpen}
                        color="inherit"
                        aria-label="open drawer"
                        className={clsx(classes.menuButton, drawerOpen && classes.hide)}
                    >
                        <MenuIcon />
                    </IconButton>
                    <Typography className={classes.title} variant="h6" noWrap>
                        LED Spy Designer
                    </Typography>
                    <ButtonGroup size="small" aria-label="small outlined button group">
                        <SerialPort class={classes.port} />
                        <Button size={'small'} disabled={disabled} onClick={fetchCfg}>
                            Load <br />
                            Config
                        </Button>
                        <Button size={'small'} disabled={disabled} onClick={writeCfg}>
                            Write
                            <br /> Config
                        </Button>
                        <Button
                            size={'small'}
                            disabled={disabled}
                            onClick={() => setFWModalOpen(true)}
                        >
                            Flash
                            <br /> Firmware
                        </Button>
                    </ButtonGroup>
                    <FormControlLabel
                        labelPlacement={'top'}
                        control={
                            <Switch
                                disabled={disabled && !inputView}
                                checked={inputView}
                                onChange={setInputViewer}
                                name="Enable Input Viewer"
                            />
                        }
                        label={'Enable Input Viewer'}
                    />
                    <div className={classes.grow} />
                    <div className={classes.sectionDesktop}></div>
                </Toolbar>
            </AppBar>
            <Sidebar />
        </div>
    );
};

export default NavBar;

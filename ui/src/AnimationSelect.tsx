import React, { Dispatch } from 'react';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import MenuItem from '@material-ui/core/MenuItem';
import * as protos from 'led-spy-protos/config';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import { useDispatch } from 'react-redux';
import { drawerWidth } from './Sidebar';
import { useCurrentAnimation } from './store/config/hooks';
import { setCurrentAnimation } from './store/config/actions';
import { map } from 'lodash';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        formControl: {
            margin: theme.spacing(1),
            width: drawerWidth - 40,
            display: 'flex',
            justifyContent: 'space-between',
        },
        selectEmpty: {
            marginTop: theme.spacing(2),
        },
        select: {
            width: drawerWidth - 40,
        },
        menuItem: {
            display: 'flex',
            justifyContent: 'space-between',
        },
        deleteButton: {},
    })
);
const AnimationSelect: React.FC = () => {
    const dispatch: Dispatch<any> = useDispatch();

    const animation = useCurrentAnimation();
    const classes = useStyles();
    const handleChange = (e: any) => {
        dispatch(setCurrentAnimation(e.target.value));
    };
    const types = {
        None: protos.Config.Profile.Animation.NONE,
        Breathing: protos.Config.Profile.Animation.BREATHE,
    };

    return (
        <div>
            <FormControl className={classes.formControl}>
                <Select
                    className={classes.select}
                    labelId="demo-simple-select-helper-label"
                    id="demo-simple-select-helper"
                    value={`${animation}`}
                    onChange={handleChange}
                >
                    {types &&
                        map(types, (tp: protos.Config.Profile.Animation, key: string) => (
                            <MenuItem key={tp} value={tp}>
                                {key}
                            </MenuItem>
                        ))}
                </Select>
            </FormControl>
        </div>
    );
};

export default AnimationSelect;

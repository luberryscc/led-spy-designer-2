import { isNumber } from 'lodash';

export const bitRead = (value: number, bit: number): boolean => ((value >> bit) & 0x01) > 0;
export const bitSet = (value: number, bit: number) => (value |= 1 << bit);
export const bitClear = (value: number, bit: number) => (value &= ~(1 << bit));
export const bitWrite = (value: number, bit: number, bitvalue: number | boolean) =>
    (isNumber(bitvalue) ? bitvalue : bitvalue ? 1 : 0) ? bitSet(value, bit) : bitClear(value, bit);

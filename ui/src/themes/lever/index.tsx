import { ReactSVG } from 'react-svg';
import React from 'react';
import lever from './images/lever.svg';
import crossup from './images/crossup.svg';
import { ThemeComponent } from '..';
export const numButtons = 20;
export const numProfiles = 10;
import './lever.css';

export const Theme: ThemeComponent = (
    { beforeInjection, afterInjection, onClick, variant },
    { ...rest }
) => {
    const injectStyles = (svg: SVGSVGElement) => {
        svg.classList.add('lever');
        beforeInjection(svg);
    };
    const variants = [
        // OTHER shouldn't be hit
        lever,
        // FIGHTSTICK
        lever,
        // STICKLESS shouldn't be hit
        lever,
        // FIGHTSTICK_EXD extra directions fightstick
        crossup,
    ];
    return (
        <ReactSVG
            src={variant ? variants[variant] : lever}
            beforeInjection={injectStyles}
            afterInjection={afterInjection}
            onClick={onClick}
            {...rest}
        />
    );
};

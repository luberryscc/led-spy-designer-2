import * as SmashBox from './smashbox';
import * as Lever from './lever';
import * as HitBox from './hitbox';
import React, { useCallback, useEffect, useState } from 'react';
import $ from 'jquery';
import _, { get, isNil, replace, startsWith, isEqual, last, noop } from 'lodash';
import * as protos from 'led-spy-protos/config';
import {
    useControllerType,
    useControllerVariant,
    useCurrentButtonStyles,
    useDrawerOpen,
    useIsInputViewer,
    useIsMapping,
    useNumButtons,
} from '../store/config/hooks';
import { useDispatch } from 'react-redux';
import { Dispatch } from 'redux';
import { createStyles, makeStyles, Snackbar, Theme as MUITheme } from '@material-ui/core';
import './themes.css';
import ButtonDialog from '../ButtonDialog';
import { drawerWidth } from '../Sidebar';
import { ButtonState, defaultButtonState } from '../store/serial';
import { socket } from '../socket';
import {
    serialError,
    setButtonState,
    setSerialError,
    setSerialMessage,
} from '../store/serial/actions';
import { protocol } from 'socket.io-client';
import ControllerVariant from '../ControllerVariant';
import {
    useSerialError,
    useSerialMessage,
    useStartingPoll,
    useStoppingPoll,
} from '../store/serial/hooks';
import MuiAlert, { AlertProps, Color } from '@material-ui/lab/Alert';
interface Props {}

interface ThemeProps {
    beforeInjection(svg: SVGSVGElement): void;
    afterInjection(err: Error, svg: SVGSVGElement): void;
    onClick(data: any): void;
    variant?: protos.Config.ControllerVariant;
}
const useStyles = makeStyles((theme: MUITheme) =>
    createStyles({
        ...{
            themeSvgBase: {
                width: '100vw',
                transition: theme.transitions.create('margin', {
                    easing: theme.transitions.easing.sharp,
                    duration: theme.transitions.duration.leavingScreen,
                }),
                marginLeft: 0,
            },
            themeSvgShift: {
                transition: theme.transitions.create('margin', {
                    easing: theme.transitions.easing.easeOut,
                    duration: theme.transitions.duration.enteringScreen,
                }),
                width: `calc(100vw - ${drawerWidth}px)`,
                marginLeft: drawerWidth,
            },
        },
    })
);

export type ThemeComponent = React.FC<ThemeProps & Record<string, any>>;
const Theme: React.FC<Props & Record<string, any>> = ({ ...rest }) => {
    const dispatch: Dispatch<any> = useDispatch();

    const themes = [
        // Generic
        SmashBox.Theme,
        // Smash Box
        SmashBox.Theme,
        // Hit Box
        HitBox.Theme,
        // Cross|up
        SmashBox.Theme,
    ];
    const isInputViewer = useIsInputViewer();
    const variantThemes = (tp: protos.Config.ControllerType) => {
        switch (tp) {
            case protos.Config.ControllerType.GENERIC: {
                return [SmashBox.Theme, Lever.Theme, HitBox.Theme, Lever.Theme];
            }
            case protos.Config.ControllerType.BBG_NOSTALGIA: {
                return [SmashBox.Theme, Lever.Theme, HitBox.Theme, Lever.Theme];
            }
        }
        return null;
    };
    const variant = useControllerVariant();

    const getControllerTheme = (ttp: protos.Config.ControllerType) => {
        const vt = variantThemes(ttp);
        return vt && variant ? vt[variant] : themes[ttp];
    };

    const controllerType = useControllerType();
    const dopen = useDrawerOpen();
    // TODO: current theme, select current theme redux
    // first make it selected based on type since we don't have a dropdown yet
    const CurrentTheme = getControllerTheme(controllerType);
    const numButtons = useNumButtons();
    const classes = useStyles();

    const [open, setOpen] = React.useState(false);
    const [clickedBtn, setClickedBtn] = React.useState(NaN);

    const handleButtonClick = (idx: number) => {
        if (isNaN(idx)) {
            return;
        }
        setClickedBtn(idx);
        setOpen(true);
    };
    const handleClick = (data: any) => {
        handleButtonClick(findButtonId(get(data, 'target', null)));
    };

    const findButtonId = (node: any): number => {
        if (isNil(node)) {
            return NaN;
        }
        const idx = getButtonId(node.id);
        if (!isNaN(idx)) {
            return idx;
        }
        return findButtonId(node.parentNode);
    };

    const getButtonId = (id: string): number => {
        if (!startsWith(id, 'btn')) {
            return NaN;
        }
        return parseInt(replace(id, 'btn', ''));
    };

    const handleClose = () => {
        setOpen(false);
        setClickedBtn(NaN);
    };
    const styles = useCurrentButtonStyles();

    const beforeInjection = (svg: SVGSVGElement) => {
        svg.classList.add(dopen ? classes.themeSvgShift : classes.themeSvgBase);
    };
    const isMapping = useIsMapping();
    const [buttonLabels, setButtonLabels] = React.useState(['']);
    const afterInjection = (_err: Error, svg: SVGSVGElement) => {
        const newLabels = [];
        for (let i = 0; i < numButtons; i++) {
            const txt = isMapping ? `${i}` : undefined;
            const text = setButtonText(i, txt);
            newLabels.push(text);
        }
        if (isEqual(buttonLabels, newLabels)) return;
        setButtonLabels(newLabels);
    };
    const setButtonText = (idx: number, text: string | undefined = undefined) => {
        const btn = $(`*[id=btn${idx}]`);
        if (!btn) {
            return '';
        }
        if (!text) {
            const nt = btn.attr('lsd-btn');
            text = nt ? nt : '';
        }
        btn.find('text').text(text);
        return text;
    };

    return (
        <React.Fragment>
            {styles.map((style) => style)}

            {!isNaN(clickedBtn) ? (
                <ButtonDialog
                    buttonId={clickedBtn}
                    numButtons={numButtons}
                    buttonLabel={buttonLabels[clickedBtn]}
                    open={open}
                    handleClose={handleClose}
                />
            ) : undefined}
            {styles && (
                <CurrentTheme
                    onClick={!isInputViewer ? handleClick : noop}
                    beforeInjection={beforeInjection}
                    afterInjection={afterInjection}
                    variant={
                        variant && controllerType == protos.Config.ControllerType.CROSSUP
                            ? protos.Config.ControllerVariant.FIGHTSTICK_EXD
                            : variant
                    }
                    {...rest}
                />
            )}
        </React.Fragment>
    );
};
export default Theme;

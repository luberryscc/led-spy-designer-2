import { ReactSVG } from 'react-svg';
import React from 'react';
import smashbox from './images/smashbox.svg';
import { ThemeComponent } from '..';
export const numButtons = 23;
export const numProfiles = 3;
import './smashbox.css';

export const Theme: ThemeComponent = (
    { beforeInjection, afterInjection, onClick },
    { ...rest }
) => {
    const injectStyles = (svg: SVGSVGElement) => {
        svg.classList.add('smash-box');
        beforeInjection(svg);
    };
    return (
        <ReactSVG
            src={smashbox}
            beforeInjection={injectStyles}
            afterInjection={afterInjection}
            onClick={onClick}
            {...rest}
        />
    );
};

import { ReactSVG } from 'react-svg';
import React from 'react';
import hitbox from './images/hitbox.svg';
import { ThemeComponent } from '..';
export const numButtons = 18;
export const numProfiles = 1;
import './hitbox.css';

export const Theme: ThemeComponent = (
    { beforeInjection, afterInjection, onClick },
    { ...rest }
) => {
    const injectStyles = (svg: SVGSVGElement) => {
        svg.classList.add('hit-box');
        beforeInjection(svg);
    };
    return (
        <ReactSVG
            src={hitbox}
            beforeInjection={injectStyles}
            afterInjection={afterInjection}
            onClick={onClick}
            {...rest}
        />
    );
};

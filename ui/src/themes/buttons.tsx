export interface ButtonHandler {
    isButtonPressed(): boolean;
}

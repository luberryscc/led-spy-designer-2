import io from 'socket.io-client';
import g from 'guark';
let sock: SocketIOClient.Socket;
export const socket = async () => {
    if (sock) {
        return sock;
    }
    const host = await g.call('socketio_host');

    sock = io(`ws://${host}`);
    sock.on('connect_error', (err: Error) => {
        setTimeout(() => {
            sock.connect();
        }, 1000);
    });
    sock.on('connect', () => console.log('connected'));
    return sock;
};

import React, { Dispatch } from 'react';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import * as protos from 'led-spy-protos/config';
import { useDispatch } from 'react-redux';
import { useControllerType, useControllerVariant } from './store/config/hooks';
import { setControllerType } from './store/config/actions';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        formControl: {
            margin: theme.spacing(1),
            width: 200,
        },
        selectEmpty: {
            marginTop: theme.spacing(2),
        },
    })
);
const ControllerType: React.FC = () => {
    const dispatch: Dispatch<any> = useDispatch();
    const variant = useControllerVariant();
    const handleChange = (e: any) => dispatch(setControllerType(e.target.value, variant));
    const types = ['Generic', 'Smash Box', 'Hit Box', 'Cross|Up', 'Bit Bang Gaming PCBS'];
    const classes = useStyles();
    const controllerType = useControllerType();
    const isTypeDisabled = (tp: protos.Config.ControllerType) =>
        tp == protos.Config.ControllerType.CROSSUP;

    return (
        <div>
            <FormControl className={classes.formControl}>
                <Select
                    labelId="demo-simple-select-helper-label"
                    id="demo-simple-select-helper"
                    value={
                        isTypeDisabled(controllerType)
                            ? protos.Config.ControllerType.SMASH_BOX
                            : controllerType
                    }
                    onChange={handleChange}
                >
                    {types &&
                        types.map((key: string, tp: protos.Config.ControllerType) => (
                            <MenuItem disabled={isTypeDisabled(tp)} key={tp} value={tp}>
                                {key}
                            </MenuItem>
                        ))}
                </Select>
            </FormControl>
        </div>
    );
};

export default ControllerType;

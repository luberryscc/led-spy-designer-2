import React, { useCallback } from 'react';
import { Dispatch, useEffect, useState } from 'react';
import { hot } from 'react-hot-loader';
import { purple } from '@material-ui/core/colors';
import NavBar from './NavBar';
import Theme from './themes';
import { createMuiTheme, ThemeProvider, CssBaseline, Snackbar } from '@material-ui/core';
import { useConfigError, useControllerVariant, useSwitchProfiles } from './store/config/hooks';
import { useDispatch } from 'react-redux';
import { setConfigError, setControllerType, setCurrentProfileId } from './store/config/actions';
import { get, isNil, trim } from 'lodash';
import { socket } from './socket';
import g from 'guark';
import * as protos from 'led-spy-protos/config';
import { setSerialError, setSerialMessage } from './store/serial/actions';
import Alert, { Color } from '@material-ui/lab/Alert';
import { useSerialError, useSerialMessage } from './store/serial/hooks';

const App = () => {
    useEffect(() => {
        g.call('is_dev').then((dev) => {
            // Disable right click menu on production, you can implement your own!
            if (!dev) {
                document.addEventListener('contextmenu', (e) => e.preventDefault());
            }
        });
    }, []);
    const darkMode = true;
    //TODO: maybe we have theme selection put it in redux store etc
    const theme = createMuiTheme({
        palette: {
            type: darkMode ? 'dark' : 'light',
            primary: { main: purple[500] },
        },
    });
    const dispatch: Dispatch<any> = useDispatch();
    const profileMap = useSwitchProfiles();
    const [prof, setProf] = useState(0);
    const variant = useControllerVariant();

    const setControllerTp = useCallback(
        (type: protos.Config.ControllerType) => {
            dispatch(setControllerType(type, variant));
        },
        [dispatch, variant]
    );

    React.useEffect(() => {
        setControllerTp(protos.Config.ControllerType.SMASH_BOX);
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    useEffect(() => {
        dispatch(setCurrentProfileId(get(profileMap, prof, 0)));
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [prof]);
    useEffect(() => {
        socket().then((sock) => {
            sock.on('event:profile', (msg: string) => setProf(parseInt(trim(msg, '"'))));
        });
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const lastSerialError = useSerialError();
    const lastConfigError = useConfigError();
    const lastMsg = useSerialMessage();
    const resetConfigError = () => {
        dispatch(setConfigError());
    };
    const resetSerialError = () => {
        dispatch(setSerialError());
    };
    const resetMsg = () => {
        dispatch(setSerialMessage());
    };
    const serialErrorSnack = () => snack(resetSerialError, 'error', lastSerialError?.message);
    const configErrorSnack = () => snack(resetConfigError, 'error', lastConfigError?.message);
    const snack = (onClose: () => void, severity?: Color, msg?: string) => (
        <Snackbar open={!isNil(msg)} autoHideDuration={6000} onClose={onClose}>
            <Alert onClose={onClose} severity={severity}>
                {msg}
            </Alert>
        </Snackbar>
    );
    const msgSnack = () => snack(resetMsg, lastMsg?.severity, lastMsg?.msg);
    return (
        <ThemeProvider theme={theme}>
            <CssBaseline>
                <div className="App">
                    {serialErrorSnack()}
                    {configErrorSnack()}
                    {msgSnack()}
                    <NavBar />
                    <header></header>
                    <Theme />
                </div>
            </CssBaseline>
        </ThemeProvider>
    );
};

export default hot(module)(App);

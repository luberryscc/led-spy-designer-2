import { combineReducers } from 'redux';

import serial from './serial';
import config from './config';

export default combineReducers({ serial, config });

import React from 'react';
import {
    Action,
    SetConfigAction,
    SetButtonConfig,
    SetConfigWriting,
    SetConfigFetching,
    SetCurrentProfileId,
    SetInputView,
    SetDrawerOpen,
    SetBrightnessRange,
    RemoveProfile,
    SetIsMapping,
    SetCurrentProfileType,
    SetCurrentAnimation,
    SetCurrentBreatheInterval,
    ConfigError,
} from './actions';
import {
    getCurrentProfile,
    isReactive,
    profileTypeString,
    getButtonStyles,
    getMappings,
    getColors,
    setColors,
    setProfile,
    setCurrentProfile,
    setDisabledButton,
    getProfile,
    getCurrentProfileId,
    setCurrentBreatheInterval,
    setConfig,
    initProfile,
    setMapping,
    error,
    setCurrentAnimation,
} from './helpers';
import * as types from './types';
import * as protos from 'led-spy-protos/config';
import { get, isNaN, isNil, concat, pullAt, cloneDeep, unset, clone } from 'lodash';
import { buttonMappingFromButtonId, profileCounts } from './types';
export interface Config {
    isFetching?: boolean;
    isWriting?: boolean;
    isMapping?: boolean;
    isInputViewer?: boolean;
    currentButtonStyles: any[];
    data?: protos.Config;
    currentProfileId?: number;
    drawerOpen: boolean;
    numButtons: number;
    lastErr?: Error;
}
export interface State {
    config: Config;
}

const colorToHex = (color: string): Uint8Array => {
    let arr: number[] = [];
    for (let i = 0; i < color.length - 1; i += 2) {
        const hex = parseInt(color.slice(i + 1, i + 3), 16);
        arr = concat(arr, hex);
    }
    if (color.length == 7) {
        arr = concat(arr, 255);
    }

    return Uint8Array.from(arr);
};

const getButtonColorsForConfig = (state: Config, a: SetButtonConfig) => {
    const buttonId = get(
        a.buttonConfig.mapping,
        'mappingId',
        buttonMappingFromButtonId(getMappings(state), a.id).mappingId
    );
    const prof = getCurrentProfile(state);

    const reactive: boolean = isReactive(prof);

    const pType = profileTypeString(prof);

    const colorBytes = colorToHex(a.buttonConfig.color);
    const rcolorBytes: Uint8Array = reactive
        ? colorToHex(a.buttonConfig.reactColor)
        : Uint8Array.from([]);
    const colors: Uint8Array = getColors(prof, pType);
    const rcolors: Uint8Array = getColors(prof, pType, true);
    const disabledButtons = prof.disabledButtons;

    return {
        colorBytes,
        rcolorBytes,
        colors,
        rcolors,
        pType,
        buttonId,
        reactive,
        disabledButtons,
    };
};
const DEFAULT_CONFIG: Config = {
    isWriting: false,
    isFetching: false,
    isInputViewer: false,
    drawerOpen: true,
    numButtons: types.buttonCounts[protos.Config.ControllerType.SMASH_BOX],
    currentButtonStyles: [],
};

const config = (state: Config = DEFAULT_CONFIG, action: Action): Config => {
    switch (action.type) {
        case types.CONFIG_ERROR: {
            const a = action as ConfigError;
            return { ...state, lastErr: a.err };
        }
        case types.ADD_PROFILE: {
            let cfg = state.data;
            if (isNil(cfg)) return error(state, 'config should not be empty');

            const numProfiles = get(cfg, 'numProfiles', 0);
            const maxProfiles =
                profileCounts[get(cfg, 'controllerType', protos.Config.ControllerType.GENERIC)];
            if (maxProfiles >= types.maxProfiles) {
                return error(
                    state,
                    `num profiles for controller type should be less than ${maxProfiles}`
                );
            }
            if (numProfiles >= maxProfiles) {
                return error(state, `num profiles should be less than ${maxProfiles}`);
            }
            const pid = getCurrentProfileId(state);
            const newProf = cloneDeep(cfg.profiles[pid]);
            cfg.profiles.push(newProf);
            cfg.numProfiles = numProfiles + 1;
            return { ...state, data: cfg, currentProfileId: numProfiles };
        }
        case types.SET_IS_MAPPING: {
            const a = action as SetIsMapping;
            return { ...state, isMapping: a.isMapping };
        }
        case types.SET_CURRENT_ANIMATION: {
            const a = action as SetCurrentAnimation;
            return setCurrentAnimation(state, a.animation);
        }
        case types.SET_CURRENT_BREATHE_INTERVAL: {
            const a = action as SetCurrentBreatheInterval;
            return setCurrentBreatheInterval(state, a.interval);
        }
        case types.SET_CURRENT_PROFILE_TYPE: {
            const a = action as SetCurrentProfileType;
            let prof = getCurrentProfile(state);
            if (!prof) return state;
            if (prof.type == a.profileType) return state;

            const pType = profileTypeString(prof);
            const colors = getColors(prof, pType);
            unset(prof, pType);

            prof.type = a.profileType;
            prof = initProfile(prof);

            const nType = profileTypeString(prof);
            const reactive = isReactive(prof);
            prof = setColors(prof, nType, colors);
            if (reactive) prof = setColors(prof, nType, clone(colors), true);

            return setCurrentProfile(state, prof);
        }
        case types.SET_CFG: {
            const a = action as SetConfigAction;
            const numButtons =
                types.buttonCounts[
                    get(a.data, 'controllerType', protos.Config.ControllerType.GENERIC)
                ];
            return setConfig({ ...state, currentProfileId: 0, numButtons }, a.data);
        }
        case types.SET_DRAWER_OPEN: {
            const a = action as SetDrawerOpen;
            return { ...state, drawerOpen: a.open };
        }
        case types.REMOVE_PROFILE: {
            const a = action as RemoveProfile;
            let cfg = state.data;
            if (isNil(cfg)) {
                return state;
            }
            const numProfiles = get(cfg, 'numProfiles', 0);
            if (numProfiles <= 0) {
                return error(state, 'num profiles should be greater than 0');
            }
            if (a.id >= numProfiles || numProfiles == 1) {
                return error(
                    state,
                    `profile ${a.id} out of range, cannot be greater than ${cfg.numProfiles}`
                );
            }

            pullAt(cfg.profiles, a.id);
            cfg.numProfiles = numProfiles - 1;
            const currentProfileId = get(state, 'currentProfileId', 0);
            const prof =
                currentProfileId >= cfg.numProfiles && currentProfileId != 0
                    ? currentProfileId - 1
                    : currentProfileId;
            return setConfig({ ...state, currentProfileId: prof }, cfg);
        }
        case types.SET_BRIGHTNESS_RANGE: {
            const a = action as SetBrightnessRange;
            let cfg = state.data;
            if (isNil(cfg)) {
                return state;
            }
            cfg.minBrightness = a.min;
            cfg.maxBrightness = a.max;
            cfg.brightness = a.curr;
            return { ...state, data: cfg };
        }
        case types.SET_ALL_BUTTONS: {
            const a = action as SetButtonConfig;
            const currentProfileId = state.currentProfileId;
            if (isNil(currentProfileId)) return error(state, 'currentProfileId must be set');
            let cfg = state.data;
            if (isNil(cfg)) return error(state, 'config should not be nil');
            const data = getButtonColorsForConfig(state, a);
            const colors: number[] = [];
            const rcolors: number[] = [];

            for (let buttonId = 0; buttonId < data.colors.length / 4; buttonId++) {
                colors.push(...data.colorBytes);
                if (data.reactive) {
                    rcolors.push(...data.rcolorBytes);
                }
            }
            let prof = getCurrentProfile(state);
            prof = setColors(prof, data.pType, Uint8Array.from(colors));
            if (data.reactive) prof = setColors(prof, data.pType, Uint8Array.from(rcolors), true);
            return setProfile(state, prof, currentProfileId);
        }
        case types.SET_BUTTON_CFG: {
            const a = action as SetButtonConfig;
            const currentProfileId = state.currentProfileId;
            if (isNil(currentProfileId)) return error(state, 'currentProfileId must be set');
            const cfg = state.data;
            if (isNil(cfg)) return error(state, 'config should not be nil');
            const newState = setMapping(state, a);

            const data = getButtonColorsForConfig(newState, a);
            const buttonId = data.buttonId;
            const colors = clone(data.colors);
            const rcolors = data.reactive ? clone(data.rcolors) : Uint8Array.from([]);
            for (let i = 0; i < 4; i++) {
                colors[buttonId * 4 + i] = data.colorBytes[i];
                if (data.reactive) {
                    rcolors[buttonId * 4 + i] = data.rcolorBytes[i];
                }
            }
            let prof = getProfile(newState, currentProfileId);
            prof = setDisabledButton(prof, a.id, a.buttonConfig.disabled);
            prof = setColors(prof, data.pType, colors);
            if (data.reactive) prof = setColors(prof, data.pType, rcolors, true);
            return setProfile(newState, prof, currentProfileId);
        }
        case types.SET_INPUT_VIEW: {
            const a = action as SetInputView;
            const newState = { ...state, isInputViewer: a.enabled };
            let err: Error | string | undefined;
            const currentButtonStyles = getButtonStyles(newState, '#btn', (e: Error | string) => {
                err = e;
            });
            if (err) return error(state, err);
            return { ...state, isInputViewer: a.enabled, currentButtonStyles };
        }
        case types.SET_WRITING_CFG: {
            const a = action as SetConfigWriting;
            return { ...state, isWriting: a.isWriting };
        }
        case types.SET_FETCHING_CFG: {
            const a = action as SetConfigFetching;
            return { ...state, isFetching: a.isFetching };
        }
        case types.SET_CURRENT_PROFILE_ID: {
            const a = action as SetCurrentProfileId;
            const numProfiles = get(state, 'data.numProfiles', NaN);
            const currentProfileId =
                isNaN(numProfiles) || a.currentProfileId < 0 || a.currentProfileId >= numProfiles
                    ? 0
                    : a.currentProfileId;
            const newState = { ...state, currentProfileId };
            let err: Error | string | undefined;
            const currentButtonStyles = getButtonStyles(newState, '#btn', (e: Error | string) => {
                err = e;
            });
            if (err) return error(state, err);
            return { ...state, currentProfileId, currentButtonStyles };
        }
        default:
            return state;
    }
};

export default config;

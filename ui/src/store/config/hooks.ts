import { RootState } from '../rootTypes';
import { useSelector } from 'react-redux';
import { get } from 'lodash';
import * as protos from 'led-spy-protos/config';
import { ButtonConfig, ButtonMapping, buttonMappingFromButtonId, profileCounts } from './types';
import {
    getButtonConfig,
    getConfigFetching,
    getControllerVariant,
    getCurrentBreatheInterval,
    getCurrentAnimation,
    getCurrentProfile,
    getCurrentProfileId,
    getMappings,
    getProfile,
    getProfileType,
    isInputViewer,
    isLever,
    isLeverList,
    isLeverVariant,
    isReactive,
} from './helpers';

export const useConfig = () =>
    useSelector((state: RootState): protos.Config | null => get(state.config, 'data', null));
export const useNumButtons = () =>
    useSelector((state: RootState): number => state.config.numButtons);
export const useMaxProfiles = () =>
    useSelector(
        (state: RootState): number =>
            profileCounts[
                get(state.config.data, 'controllerType', protos.Config.ControllerType.GENERIC)
            ]
    );
export const useConfigWriting = () =>
    useSelector((state: RootState): boolean => get(state.config, 'isWriting', false));
export const useDrawerOpen = () =>
    useSelector((state: RootState): boolean => get(state.config, 'drawerOpen', false));
export const useConfigFetching = () =>
    useSelector((state: RootState): boolean => getConfigFetching(state.config));

export const useControllerType = () =>
    useSelector(
        (state: RootState): protos.Config.ControllerType =>
            get(state.config, 'data.controllerType', protos.Config.ControllerType.GENERIC)
    );
export const useControllerVariant = () =>
    useSelector((state: RootState): protos.Config.ControllerVariant | undefined =>
        getControllerVariant(state.config)
    );

export const useBrighnessRange = (): number[] =>
    useSelector((state: RootState) => {
        const minBright = get(state.config, 'data.minBrightness', 0);
        const maxBright = get(state.config, 'data.maxBrightness', 1);
        const currBright = get(state.config, 'data.brightness', 0.5);
        return [minBright, currBright, maxBright];
    });

export const useNumProfiles = () =>
    useSelector(
        (state: RootState): protos.Config.ControllerType =>
            get(state.config, 'data.numProfiles', protos.Config.ControllerType.GENERIC)
    );
export const useCurrentProfileId = () =>
    useSelector((state: RootState): number => getCurrentProfileId(state.config));

export const useProfile = (profileId: number) =>
    useSelector((state: RootState): protos.Config.Profile => getProfile(state.config, profileId));
export const useProfileType = (profileId: number) =>
    useSelector(
        (state: RootState): protos.Config.ProfileType => getProfileType(state.config, profileId)
    );
export const useCurrentProfileType = () =>
    useSelector(
        (state: RootState): protos.Config.ProfileType =>
            getProfileType(state.config, getCurrentProfileId(state.config))
    );

export const useCurrentProfile = () =>
    useSelector((state: RootState): protos.Config.Profile => getCurrentProfile(state.config));
export const useMappings = () =>
    useSelector((state: RootState): Uint8Array => getMappings(state.config));

export const useCurrentButtonStyles = () =>
    useSelector((state: RootState): any[] => state.config.currentButtonStyles);

export const useButtonConfig = (btnId: number) =>
    useSelector((state: RootState): ButtonConfig => getButtonConfig(state.config, btnId));

export const useButtonConfigs = (numButtons: number) =>
    useSelector((state: RootState): ButtonConfig[] => {
        const cfgs: ButtonConfig[] = [];
        for (let i = 0; i < numButtons; i++) cfgs.push(getButtonConfig(state.config, i));

        return cfgs;
    });

export const useIsMapping = () =>
    useSelector((state: RootState): boolean => get(state.config, 'isMapping', false));

export const useButtonMapping = (btnId: number) =>
    useSelector((state: RootState): ButtonMapping => {
        const mappings = getMappings(state.config);
        return buttonMappingFromButtonId(mappings, btnId);
    });

export const useIsInputViewer = () =>
    useSelector((state: RootState): boolean => isInputViewer(state.config));

export const useReactive = () =>
    useSelector((state: RootState): boolean => isReactive(getCurrentProfile(state.config)));
export const useSwitchMapping = (id: number) =>
    useSelector((state: RootState): number => get(state.config.data, `switchProfiles[${id}]`, 0));
export const useSwitchProfiles = () =>
    useSelector(
        (state: RootState): Uint8Array =>
            get(state.config.data, `switchProfiles`, Uint8Array.from([0, 0, 0]))
    );
export const useCurrentAnimation = () =>
    useSelector((state: RootState): number => getCurrentAnimation(state.config));

export const useCurrentBreatheInterval = () =>
    useSelector((state: RootState): number => getCurrentBreatheInterval(state.config));

export const useConfigError = () =>
    useSelector((state: RootState): Error | undefined => get(state, 'config.lastErr'));

export const useIsLever = (idx: number) =>
    useSelector((state: RootState) => isLever(state.config, idx));

export const useIsLeverList = () => useSelector((state: RootState) => isLeverList(state.config));
export const useIsLeverVariant = () =>
    useSelector((state: RootState) => isLeverVariant(state.config));

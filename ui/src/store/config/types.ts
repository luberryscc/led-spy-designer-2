import { get, indexOf } from 'lodash';
export const SET_FETCHING_CFG = 'SET_FETCHING_PORTS';
export const SET_CFG = 'SET_CFG';
export const SET_WRITING_CFG = 'SET_WRITING_CFG';
export const GET_BUTTON_CFG = 'GET_BUTTON_CFG';
export const SET_BUTTON_CFG = 'SET_BUTTON_CFG';
export const SET_ALL_BUTTONS = 'SET_ALL_BUTTONS';
export const SET_CURRENT_PROFILE_ID = 'SET_CURRENT_PROFILE_ID';
export const ADD_PROFILE = 'ADD_PROFILE';
export const REMOVE_PROFILE = 'REMOVE_PROFILE';
export const CONFIG_ERROR = 'CONFIG_ERROR';
export const GET_DEFAULT_CONFIG = 'GET_DEFAULT_CONFIG';
export const SET_CONTROLLER_TYPE = 'SET_CONTROLLER_TYPE';
export const SET_INPUT_VIEW = 'SET_INPUT_VIEW';
export const SET_DRAWER_OPEN = 'SET_DRAWER_OPEN';
export const SET_BRIGHTNESS_RANGE = 'SET_BRIGHTNESS_RANGE';
export const SET_IS_MAPPING = 'SET_IS_MAPPING';
export const SET_CURRENT_PROFILE_TYPE = 'SET_CURRENT_PROFILE_TYPE';
export const SET_CURRENT_ANIMATION = 'SET_CURRENT_ANIMATION';
export const SET_CONTROLLER_VARIANT = 'SET_CONTROLLER_VARIANT';
export const SET_CURRENT_BREATHE_INTERVAL = 'SET_CURRENT_BREATHE_INTERVAL';
export const SET_NUM_BUTTONS = 'SET_NUM_BUTTONS';
import { numButtons as sbxButtons, numProfiles as sbxProfiles } from '../../themes/smashbox';
// TODO: add mapping settings, and also the fancy button testing stuff for mapping.

export type ButtonMapping = {
    buttonId: number;
    mappingId: number;
};
export const buttonMappingFromMappingId = (
    mappings: Uint8Array,
    mappingId: number
): ButtonMapping => ({
    mappingId,
    buttonId: get(mappings, mappingId, -1),
});
export const buttonMappingFromButtonId = (
    mappings: Uint8Array,
    buttonId: number
): ButtonMapping => ({
    buttonId,
    mappingId: indexOf(mappings, buttonId),
});

export type ButtonConfig = {
    color: string;
    reactColor: string;
    rawColor: Uint8Array;
    rawReactColor: Uint8Array;
    mapping?: ButtonMapping;
    disabled?: boolean;
};
export const DEFAULT_BUTTON_CONFIG = {
    color: '#000000FF',
    reactColor: '#000000FF',
    rawColor: Uint8Array.from([]),
    rawReactColor: Uint8Array.from([]),
};

export const breathInterval = {
    min: 0.0001, //slowest
    max: 0.009, //fastest
    default: 0.001, //sane default
    step: 0.0001,
};

export const maxProfiles = 12;

// TODO: use imports for these
const genButtons = 29;
const hbxButtons = 18;
const xuButtons = 20;
const bbgButtons = 19;
export const buttonCounts = [
    // Generic
    genButtons,
    // Smash Box
    sbxButtons,
    // Hit Box
    hbxButtons,
    // Cross|up
    xuButtons,
    // BBG Nostalgia
    bbgButtons,
];
const genProfiles = maxProfiles;
// for now
const hbxProfiles = 1;
const xuProfiles = maxProfiles;
const bbgProfiles = 3;

export const profileCounts = [
    // Generic
    genProfiles,
    // Smash Box
    sbxProfiles,
    // Hit Box
    hbxProfiles,
    // Cross|up
    xuProfiles,
    // BBG
    bbgProfiles,
];

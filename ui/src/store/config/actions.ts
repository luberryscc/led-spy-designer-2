// store/session/actions.ts
import { ThunkAction, ThunkDispatch } from 'redux-thunk';
import { AnyAction } from 'redux';
import g from 'guark';
import * as types from './types';
import * as protos from 'led-spy-protos/config';
import { base64ToBytes, bytesToBase64 } from 'byte-base64';
import { setSerialMessage, startPoll, stopPoll } from '../serial/actions';
import { endsWith } from 'lodash';
//import { fstat, readFile, writeFile } from 'fs';

export type SetConfigAction = {
    type: string;
    data: protos.Config;
};

export type SetConfigFetching = {
    type: string;
    isFetching: boolean;
};

export type SetConfigWriting = {
    type: string;
    isWriting: boolean;
};

export type SetInputView = {
    type: string;
    enabled: boolean;
};
export type ConfigError = {
    type: string;
    err?: Error;
};

export type SetDrawerOpen = {
    type: string;
    open: boolean;
};

export type SetBrightnessRange = {
    type: string;
    max: number;
    min: number;
    curr: number;
};

export type SetCurrentBreatheInterval = {
    type: string;
    interval: number;
};

export type SetCurrentProfileId = {
    type: string;
    currentProfileId: number;
};

export type SetIsMapping = {
    type: string;
    isMapping: boolean;
};
export type AddProfile = {
    type: string;
};
export type RemoveProfile = {
    type: string;
    id: number;
};
export type SetNumButtons = {
    type: string;
    numButtons: number;
};

export type SetControllerType = {
    type: string;
    controllerType: protos.Config.ControllerType;
};
export type SetCurrentProfileType = {
    type: string;
    profileType: protos.Config.ProfileType;
};
export type SetCurrentAnimation = {
    type: string;
    animation: protos.Config.Profile.Animation;
};
export type SetButtonConfig = {
    type: string;
    id: number;
    buttonConfig: types.ButtonConfig;
};

export type Action =
    | SetConfigFetching
    | SetConfigWriting
    | SetConfigAction
    | SetControllerType
    | SetCurrentProfileId
    | SetInputView
    | SetButtonConfig
    | SetDrawerOpen
    | SetBrightnessRange
    | RemoveProfile
    | AddProfile
    | SetCurrentProfileType
    | SetCurrentAnimation
    | SetCurrentBreatheInterval
    | SetIsMapping
    | SetNumButtons
    | ConfigError;

export const setConfig = (config: protos.Config): SetConfigAction => {
    return { type: types.SET_CFG, data: config };
};
export const setNumButtons = (numButtons: number): SetNumButtons => {
    return { type: types.SET_NUM_BUTTONS, numButtons };
};
export const setButtonConfig = (id: number, buttonConfig: types.ButtonConfig): SetButtonConfig => {
    return { type: types.SET_BUTTON_CFG, id, buttonConfig };
};
export const setAllButtons = (id: number, buttonConfig: types.ButtonConfig): SetButtonConfig => {
    return { type: types.SET_ALL_BUTTONS, id, buttonConfig };
};
export const isMapping = (isMapping: boolean) => {
    return { type: types.SET_IS_MAPPING, isMapping };
};

export const isFetchingConfig = (isFetching: boolean): SetConfigFetching => {
    return { type: types.SET_FETCHING_CFG, isFetching };
};
export const setBrightnessRange = (min: number, max: number, curr: number): SetBrightnessRange => {
    return { type: types.SET_BRIGHTNESS_RANGE, min, max, curr };
};
export const removeProfile = (id: number): RemoveProfile => {
    return { type: types.REMOVE_PROFILE, id };
};

export const addProfile = (): AddProfile => {
    return { type: types.ADD_PROFILE };
};

export const isWritingConfig = (isWriting: boolean): SetConfigWriting => {
    return { type: types.SET_WRITING_CFG, isWriting };
};
export const isDrawerOpen = (open: boolean): SetDrawerOpen => {
    return { type: types.SET_DRAWER_OPEN, open };
};

export const setCurrentProfileType = (
    profileType: protos.Config.ProfileType
): SetCurrentProfileType => {
    return { type: types.SET_CURRENT_PROFILE_TYPE, profileType };
};

export const setCurrentAnimation = (
    animation: protos.Config.Profile.Animation
): SetCurrentAnimation => {
    return { type: types.SET_CURRENT_ANIMATION, animation };
};

export const setCurrentBreatheInterval = (interval: number): SetCurrentBreatheInterval => {
    return { type: types.SET_CURRENT_BREATHE_INTERVAL, interval };
};
export const isInputViewer = (
    enabled: boolean,
    numButtons: number = 0,
    type: protos.Config.ControllerType = protos.Config.ControllerType.GENERIC,
    variant?: protos.Config.ControllerVariant
): ThunkAction<Promise<void>, {}, {}, AnyAction> => {
    // Invoke API
    return async (dispatch: ThunkDispatch<{}, {}, AnyAction>): Promise<void> => {
        return new Promise<void>((resolve) => {
            dispatch(
                enabled
                    ? startPoll(type, numButtons, variant, () => dispatch(configReset()))
                    : stopPoll(() => dispatch(configReset()))
            );
            dispatch(setIsInputViewer(enabled));
        });
    };
};

export const setIsInputViewer = (enabled: boolean): SetInputView => {
    //TODO: close this shit when enabled is set to false
    return { type: types.SET_INPUT_VIEW, enabled };
};

export const setCurrentProfileId = (currentProfileId: number) => {
    return { type: types.SET_CURRENT_PROFILE_ID, currentProfileId };
};

export const writeConfig = (
    config: protos.Config
): ThunkAction<Promise<void>, {}, {}, AnyAction> => {
    // Invoke API
    return async (dispatch: ThunkDispatch<{}, {}, AnyAction>): Promise<void> => {
        return new Promise<void>((resolve) => {
            setSerialMessage('writing config to controller');
            dispatch(isWritingConfig(true));
            dispatch(isInputViewer(false));
            try {
                const cfg = protos.Config.fromObject(config);
                const raw_cfg = bytesToBase64(protos.Config.encode(cfg).finish());
                g.call('write_raw_config', { raw_cfg })
                    .then(() => {
                        dispatch(setSerialMessage('config written to controller', 'success'));
                        dispatch(isWritingConfig(false));
                    })
                    .catch((err: Error) => dispatch(configError(err)));
            } catch (e: any) {
                dispatch(configError(e));
            }
        });
    };
};
export const setConfigError = (err?: Error): ConfigError => {
    return { type: types.CONFIG_ERROR, err };
};
export const configError = (err: Error): ThunkAction<Promise<void>, {}, {}, AnyAction> => {
    // Invoke API
    return async (dispatch: ThunkDispatch<{}, {}, AnyAction>): Promise<void> => {
        return new Promise<void>((resolve) => {
            dispatch(configReset);
            dispatch(setConfigError(err));
        });
    };
};
export const configReset = (): ThunkAction<Promise<void>, {}, {}, AnyAction> => {
    // Invoke API
    return async (dispatch: ThunkDispatch<{}, {}, AnyAction>): Promise<void> => {
        return new Promise<void>((resolve) => {
            dispatch(isFetchingConfig(false));
            dispatch(isMapping(false));
            dispatch(isWritingConfig(false));
        });
    };
};

const addExtension = (file: string): string => {
    const extension = '.lspy';
    if (endsWith(file, extension)) return file;
    return file + extension;
};
export const saveConfigToFile = (
    file: string,
    config: protos.Config
): ThunkAction<Promise<void>, {}, {}, AnyAction> => {
    // Invoke API
    return async (dispatch: ThunkDispatch<{}, {}, AnyAction>): Promise<void> => {
        return new Promise<void>((resolve) => {
            dispatch(isWritingConfig(true));
            dispatch(isInputViewer(false));
            try {
                const cfg = protos.Config.fromObject(config);
                const raw_cfg = bytesToBase64(protos.Config.encode(cfg).finish());
                g.call('write_config_file', { file: addExtension(file), raw_cfg })
                    .then(() => {
                        dispatch(setSerialMessage('successfully saved config to file', 'success'));
                        dispatch(isWritingConfig(false));
                    })
                    .catch((err: Error) => dispatch(configError(err)));
            } catch (e: any) {
                dispatch(configError(e));
            }
        });
    };
};
export const readConfigFromFile = (file: string): ThunkAction<Promise<void>, {}, {}, AnyAction> => {
    // Invoke API
    return async (dispatch: ThunkDispatch<{}, {}, AnyAction>): Promise<void> => {
        return new Promise<void>((resolve) => {
            dispatch(isFetchingConfig(true));
            dispatch(isInputViewer(false));
            g.call('read_config_file', { file }).then((data: string) => {
                try {
                    const d = base64ToBytes(data);
                    const cfg = protos.Config.decode(d);
                    dispatch(setConfig(cfg));
                    dispatch(setSerialMessage('loaded config from file'));
                    dispatch(isFetchingConfig(false));
                } catch (e: any) {
                    dispatch(configError(e));
                }
            });
        });
    };
};
export const fetchConfig = (): ThunkAction<Promise<void>, {}, {}, AnyAction> => {
    // Invoke API
    return async (dispatch: ThunkDispatch<{}, {}, AnyAction>): Promise<void> => {
        return new Promise<void>((resolve) => {
            dispatch(isFetchingConfig(true));
            dispatch(isInputViewer(false));

            g.call('read_raw_config')
                .then((data: string) => {
                    try {
                        const d = base64ToBytes(data);
                        const cfg = protos.Config.decode(d);
                        dispatch(setConfig(cfg));
                        dispatch(setSerialMessage('loaded config from contoller'));
                        dispatch(isFetchingConfig(false));
                    } catch (e: any) {
                        dispatch(configError(e));
                    }
                })
                .catch((e: Error) => dispatch(configError(e)));
        });
    };
};

// sets controller type and gets default config for that controller
export const setControllerType = (
    tp: protos.Config.ControllerType,
    variant?: protos.Config.ControllerVariant
): ThunkAction<Promise<void>, {}, {}, AnyAction> => {
    // Invoke API
    return async (dispatch: ThunkDispatch<{}, {}, AnyAction>): Promise<void> => {
        return new Promise<void>((resolve) => {
            dispatch(isFetchingConfig(true));
            dispatch(isInputViewer(false));
            g.call('get_default_config', { type: tp, variant: variant })
                .then((data: string) => {
                    try {
                        const d = base64ToBytes(data);
                        const cfg = protos.Config.decode(d);
                        dispatch(setConfig(cfg));
                        dispatch(isFetchingConfig(false));
                    } catch (e: any) {
                        dispatch(configError(e));
                    }
                })
                .catch((e: Error) => dispatch(configError(e)));
        });
    };
};

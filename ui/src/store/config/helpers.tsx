import React from 'react';
import { cloneDeep, concat, get, indexOf, isEmpty, isNil, isNull, set, unset } from 'lodash';
import { Config } from '.';
import * as protos from 'led-spy-protos/config';
import { base64ToBytes } from 'byte-base64';
import { buttonMappingFromButtonId, DEFAULT_BUTTON_CONFIG } from './types';
import { bitRead, bitWrite } from '../../util/bits';
import { Buffer } from 'buffer';
import { SetButtonConfig } from './actions';

// Profile Helpers
export const getProfile = (state: Config, profileId: number): protos.Config.Profile => {
    return cloneDeep(get(state, `data.profiles[${profileId}]`));
};

export const getCurrentProfileId = (state: Config): number => {
    return get(state, 'currentProfileId', 0);
};

export const getProfileType = (state: Config, profileId: number): protos.Config.ProfileType => {
    return get(getProfile(state, profileId), 'type');
};

export const getCurrentProfile = (state: Config): protos.Config.Profile => {
    const profileId = getCurrentProfileId(state);
    const prof = getProfile(state, profileId);
    return prof;
};

const profileSettings = {
    Static: 'pstatic',
    Reactive: 'preactive',
};
export const isStatic = (prof: protos.Config.Profile | null | undefined): boolean =>
    !isNull(get(prof, profileSettings.Static, null));

export const isReactive = (prof: protos.Config.Profile | null | undefined): boolean =>
    !isNull(get(prof, profileSettings.Reactive, null));

export const initProfile = (prof: protos.Config.Profile): protos.Config.Profile => {
    const p = prof;
    switch (prof.type) {
        case protos.Config.ProfileType.REACTIVE: {
            p.preactive = new protos.Config.Profile.Reactive({
                colors: Uint8Array.from([]),
                reactColors: Uint8Array.from([]),
            });
            break;
        }
        case protos.Config.ProfileType.STATIC: {
            p.pstatic = new protos.Config.Profile.Static({
                colors: Uint8Array.from([]),
            });
            break;
        }
    }
    return p;
};

export const profileTypeString = (prof: protos.Config.Profile | null | undefined): string => {
    if (isStatic(prof)) {
        return profileSettings.Static;
    } else if (isReactive(prof)) {
        return profileSettings.Reactive;
    }
    return '';
};

// ButtonHelpers

export const getColorFromKey = (
    prof: protos.Config.Profile | null | undefined,
    btnId: number,
    key: string
) => {
    const data = get(prof, `${key}`);
    if (isEmpty(data)) {
        return { string: '#000000FF', raw: Uint8Array.from([0, 0, 0, 255]) };
    }
    const getData = () => {
        switch (data.type) {
            case String: {
                return base64ToBytes(data);
            }
            default: {
                return data;
            }
        }
    };
    const bytes = getData();
    return colorFromHex(btnId * 4, Buffer.from(bytes));
};
const colorFromHex = (i: number, arr: Buffer) => {
    const toHex = (n: number) => {
        var s = n.toString(16);
        return n < 10 ? '0' + s : s;
    };
    if (arr.length < i + 4) {
        throw TypeError(
            'not a valid binary encoded color: too short at index ' + i + ': length ' + arr.length
        );
    }
    return {
        string: '#' + toHex(arr[i]) + toHex(arr[i + 1]) + toHex(arr[i + 2]) + toHex(arr[i + 3]),
        raw: Uint8Array.from([arr[i], arr[i + 1], arr[i + 2], arr[i + 3]]),
    };
};

export const getColors = (prof: protos.Config.Profile, pType: string, react: boolean = false) =>
    get(prof, `${pType}.${react ? 'reactColors' : 'colors'}`, Uint8Array.from([]));

export const setColors = (
    prof: protos.Config.Profile,
    pType: string,
    colors: Uint8Array,
    react: boolean = false
) => set(prof, `${pType}.${react ? 'reactColors' : 'colors'}`, colors);

export const getButtonConfig = (
    state: Config,
    btnId: number,
    onError?: (err: Error | string) => void
) => {
    if (isNaN(btnId)) return DEFAULT_BUTTON_CONFIG;
    const currentProfile = getCurrentProfile(state);
    const disabledButtons = get(currentProfile, 'disabledButtons', 0);
    const disabled = bitRead(disabledButtons, btnId);
    const pType = profileTypeString(currentProfile);
    const mappings = getMappings(state);
    const btn = buttonMappingFromButtonId(mappings, btnId);
    try {
        const color = getColorFromKey(currentProfile, btn.mappingId, `${pType}.colors`);
        const reactColor = isReactive(currentProfile)
            ? getColorFromKey(currentProfile, btn.mappingId, `${pType}.reactColors`)
            : color;
        return {
            color: color.string,
            rawColor: color.raw,
            reactColor: reactColor.string,
            rawReactColor: reactColor.raw,
            mapping: btn,
            disabled,
        };
    } catch (e: any) {
        onError ? onError(e) : console.error(e);
    }
    return DEFAULT_BUTTON_CONFIG;
};
const variant = (v: protos.Config.ControllerVariant) => (v > 1 ? v : 1);
// TODO: generic by default?
export const getControllerVariant = (state: Config) => get(state, 'data.variant');

const leverVariants = [
    // OTHER
    false,
    // FIGHTSTICK
    true,
    // STICKLESS
    false,
    // FIGHTSTICK_EXD
    true,
];
export const isLever = (state: Config, idx: number) =>
    isLeverVariant(state) && idx >= 8 && idx <= 11;
export const isLeverVariant = (state: Config) => leverVariants[getControllerVariant(state)];
export const isLeverList = (state: Config) => {
    const numButtons = state.numButtons;
    const list: boolean[] = [];
    for (let i = 0; i < numButtons; i++) {
        list.push(isLever(state, i));
    }
    return list;
};

const getButtonStyle = (
    state: Config,
    idx: number,
    idPrefix: string,
    onError?: (err: Error | string) => void
) => {
    const currentProfile = getCurrentProfile(state);
    const reactive = isReactive(currentProfile);
    const buttonConfig = getButtonConfig(state, idx, onError);
    // TODO: regen on inputView
    const idStr = `${idPrefix}${idx}`;
    const ringColor = reactive ? buttonConfig.reactColor : buttonConfig.color;
    const rawRingColor = reactive ? buttonConfig.rawReactColor : buttonConfig.rawColor;
    const inputView = get(state, 'isInputViewer', false);
    const color = buttonConfig.color;
    const rawColor = buttonConfig.rawColor;
    // TODO: have a toggle
    const displayLabel = 'inherit';
    const disabled = !inputView && get(buttonConfig, 'disabled', false);
    const downTextColor = contrastingColor(
        disabled ? Uint8Array.from([0, 0, 0, 255]) : rawRingColor
    );
    const isALever = isLever(state, idx);
    const outlineBase = '#000000';
    const upTextColor = contrastingColor(disabled ? Uint8Array.from([0, 0, 0, 255]) : rawColor);
    const outlineOpacity = 0.5;
    const arrowOpacity = 0.5;
    const outlineColor = (color: string) =>
        color.toLowerCase() == '#000000ff' ? outlineBase : color;
    const overrideColor = (color: string) =>
        color.toLowerCase() == '#000000ff' ? upTextColor : color;
    return isALever ? (
        // TODO: add stick animations
        <style key={idx}>{`
            ${idStr} .outline {
                fill: ${
                    inputView ? outlineColor(color) : disabled ? outlineBase : outlineColor(color)
                }!important;
                fill-opacity:${inputView ? 'initial' : 1}!important;
                stroke: ${
                    inputView ? upTextColor : disabled ? outlineBase : overrideColor(ringColor)
                }!important;
                stroke-opacity:${inputView ? outlineOpacity : 1}!important;
            }
            ${idStr} .outline.active {
                fill:${ringColor}!important;
                stroke: ${downTextColor}!important;
            }
            ${idStr} .arrow { 
                fill:${
                    inputView ? upTextColor : disabled ? '#000000FF' : outlineColor(ringColor)
                }!important;
                fill-opacity:${inputView ? arrowOpacity : 1}!important;
            } 
            ${idStr} .arrow.active {
                fill:${downTextColor}!important;
                fill-opacity:${inputView ? arrowOpacity : 1}!important;
            }
            `}</style>
    ) : (
        <style key={idx}>{`
            ${idStr} .ring {
                fill:${inputView ? color : disabled ? '#000000FF' : ringColor};
            }
            ${idStr} .ring.active {
                fill:${ringColor};
            }
            ${idStr} .plunger { 
                fill:${disabled ? '#000000FF' : color};
            } 
            ${idStr} .plunger.active { 
                fill:${ringColor};
            } 
            ${idStr} .plungerup.active {
                display: none;
            }
            ${idStr} .plungerdown.active {
                display: inline;
            }
            ${idStr} .plunger-text{
                display: ${displayLabel};
                fill: ${upTextColor};
            }
            ${idStr} .plunger-text.active {
                display: ${displayLabel};
                fill: ${downTextColor};
            }
            `}</style>
    );
};

export const getButtonStyles = (
    state: Config,
    idPrefix: string,
    onError?: (err: Error | string) => void
) => {
    let styles: any[] = [];
    for (let i = 0; i < state.numButtons; i++) {
        styles = concat(styles, getButtonStyle(state, i, idPrefix, onError));
    }
    return styles;
};

// Mapping Helpers
export const getMappings = (state: Config): Uint8Array => {
    const mappings = get(state, 'data.mappings', '');
    switch (mappings.type) {
        case String: {
            return base64ToBytes(mappings);
        }
        default: {
            return mappings;
        }
    }
};

// Fetching Helpers
export const getConfigFetching = (state: Config): boolean => {
    return get(state, 'isFetching', false);
};
const luma = (color: Uint8Array) => {
    const rgb = color;
    return 0.2126 * rgb[0] + 0.7152 * rgb[1] + 0.0722 * rgb[2]; // SMPTE C, Rec. 709 weightings
};
const contrastingColor = (color: Uint8Array) => {
    return luma(color) >= 165 ? '#000' : '#fff';
};

export const isInputViewer = (state: Config) => get(state, 'isInputViewer', false);

export const setCurrentProfile = (state: Config, prof: protos.Config.Profile) => {
    const pid = getCurrentProfileId(state);
    return setProfile(state, prof, pid);
};
export const setProfile = (state: Config, prof: protos.Config.Profile, pid: number) => {
    if (isNil(state.data)) return state;
    const newCfg = set(state.data, `profiles[${pid}]`, prof);
    return setConfig(state, newCfg);
};

export const setConfig = (state: Config, cfg: protos.Config) => {
    const newState = set(state, 'data', cfg);
    let err: Error | string | undefined;
    const currentButtonStyles = getButtonStyles(newState, '#btn', (e: Error | string) => {
        err = e;
    });
    if (err) return error(state, err);

    return { ...newState, currentButtonStyles };
};

export const setDisabledButton = (
    prof: protos.Config.Profile,
    id: number,
    disable: boolean = false
) => {
    const disabled = bitWrite(prof.disabledButtons, id, disable);
    return set(prof, `disabledButtons`, disabled);
};
export const setMapping = (state: Config, a: SetButtonConfig) => {
    const mappingId = get(a.buttonConfig.mapping, `mappingId`, 255);
    let mappings = getMappings(state);
    const old = indexOf(mappings, a.id);
    mappings = set(mappings, `[${old}]`, get(mappings, `[${mappingId}]`, 255));
    mappings = set(mappings, `[${mappingId}]`, a.id);
    return set(state, `data.mappings`, mappings);
};

export const getBreatheInterval = (state: Config, pid: number) =>
    get(getProfile(state, pid), 'breatheInterval', 0);
export const getCurrentBreatheInterval = (state: Config) =>
    get(getCurrentProfile(state), 'breatheInterval', 0);
export const getCurrentAnimation = (state: Config) => get(getCurrentProfile(state), 'animation', 0);
export const setCurrentBreatheInterval = (state: Config, int: number) =>
    setBreatheInterval(state, getCurrentProfileId(state), int);

export const setBreatheInterval = (state: Config, pid: number, int: number) => {
    if (isNil(state.data)) return error(state, 'config should not be empty');
    if (int < 0) return error(state, `interval ${int} should be greater than or equal to 0`);

    return set(state, `data.profiles[${pid}].breatheInterval`, int);
};
export const setCurrentAnimation = (state: Config, anim: protos.Config.Profile.Animation) =>
    setAnimation(state, getCurrentProfileId(state), anim);

export const setAnimation = (
    state: Config,
    pid: number,
    animation: protos.Config.Profile.Animation
) => {
    if (isNil(state.data)) return error(state, 'config should not be empty');
    if (animation == protos.Config.Profile.Animation.NIL)
        return error(state, `invalid value ${animation} for animation`);
    return set(state, `data.profiles[${pid}].animation`, animation);
};

export const getAnimation = (prof: protos.Config.Profile) => prof.animation;
export const supportsVariant = (tp: protos.Config.ControllerType) =>
    tp == protos.Config.ControllerType.GENERIC || tp == protos.Config.ControllerType.BBG_NOSTALGIA;
export const error = (state: Config, msg: string | Error) => {
    const lastErr = msg instanceof Error ? msg : Error(msg);
    return { ...state, lastErr };
};

// store/session/actions.ts
import { ThunkAction, ThunkDispatch } from 'redux-thunk';
import { AnyAction } from 'redux';
import g from 'guark';
import * as types from './types';
import * as protos from 'led-spy-protos/config';
import { concat } from 'lodash';
import { base64decode } from 'byte-base64';
import { ButtonState, defaultButtonState } from '.';
import { isInputViewer, RemoveProfile } from '../config/actions';
import { Color } from '@material-ui/lab/Alert';

export type SetPortsAction = {
    type: string;
    ports: string | string[];
};

export type SetPortsFetching = {
    type: string;
    isFetching: boolean;
};

export type SetPortAction = {
    type: string;
    currentPort: string;
};
export type SetPortSetting = {
    type: string;
    isSetting: boolean;
};

export type SetButtonStateAction = {
    type: string;
    buttons: boolean[];
    dpadSwitch: boolean;
    profile: number;
    lSX: number;
    lSY: number;
};

export type SetButtonStateFetching = {
    type: string;
    isFetching: boolean;
};

export type SerialError = {
    type: string;
    err?: Error;
};
export type SerialMessage = {
    type: string;
    msg?: types.Message;
};

export type SerialClosing = {
    type: string;
    closing: boolean;
};
export type SetStopPollAction = {
    type: string;
    stopping: boolean;
};
export type SetStartPollAction = {
    type: string;
    starting: boolean;
};
export type SerialFlashing = {
    type: string;
    flashing: boolean;
};

export type Action =
    | SetPortsAction
    | SetPortsFetching
    | SetPortAction
    | SetPortSetting
    | SetButtonStateFetching
    | SetButtonStateAction
    | SerialClosing
    | SetStopPollAction
    | SetStartPollAction
    | SerialError
    | RemoveProfile
    | SerialFlashing
    | SerialMessage;

export const setPorts = (ports: string | string[]): SetPortsAction => {
    const data: string[] = concat([], ports);
    return { type: types.SET_PORTS, ports: data };
};
export const isFetchingPorts = (isFetching: boolean): SetPortsFetching => {
    return { type: types.SET_FETCHING_PORTS, isFetching };
};

export const isFetchingButtonState = (isFetching: boolean): SetButtonStateFetching => {
    return { type: types.SET_FETCHING_BUTTON_STATE, isFetching };
};

export const isSettingPort = (isSetting: boolean): SetPortSetting => {
    return { type: types.SET_SETTING_PORT, isSetting };
};

export const isClosing = (closing: boolean): SerialClosing => {
    return { type: types.SERIAL_CLOSING, closing };
};
export const isFlashing = (flashing: boolean): SerialFlashing => {
    return { type: types.SERIAL_FLASHING, flashing };
};
export const isStoppingPoll = (stopping: boolean): SetStopPollAction => {
    return { type: types.SET_STOPPING_POLL, stopping };
};
export const isStartingPoll = (starting: boolean): SetStartPollAction => {
    return { type: types.SET_STARTING_POLL, starting };
};

export const setPortValue = (port: string): SetPortAction => {
    return { type: types.SET_PORT, currentPort: port };
};
export const setPort = (port: string): ThunkAction<Promise<void>, {}, {}, AnyAction> => {
    // Invoke API
    return async (dispatch: ThunkDispatch<{}, {}, AnyAction>): Promise<void> => {
        return new Promise<void>((resolve) => {
            dispatch(isSettingPort(true));
            g.call('set_serial_port', { port })
                .then(() => {
                    dispatch(setPortValue(port));
                    dispatch(isSettingPort(false));
                })
                .catch((err: Error) => dispatch(setSerialError(err)));
        });
    };
};

export const fetchPorts = (): ThunkAction<Promise<void>, {}, {}, AnyAction> => {
    // Invoke API
    return async (dispatch: ThunkDispatch<{}, {}, AnyAction>): Promise<void> => {
        return new Promise<void>((resolve) => {
            dispatch(isFetchingPorts(true));

            g.call('list_serial_ports')
                .then((data: string[] | string) => {
                    dispatch(setPorts(data));
                    dispatch(isFetchingPorts(false));
                })
                .catch((err: Error) => dispatch(setSerialError(err)));
        });
    };
};

export const setButtonState = (data: ButtonState): SetButtonStateAction => {
    return { type: types.SET_BUTTON_STATE, ...data };
};

export const setSerialError = (err?: Error): SerialError => {
    return { type: types.SERIAL_ERROR, err };
};
export const setSerialMessage = (msg?: string, severity?: Color): SerialMessage => {
    const message = msg ? { msg, severity: severity || 'info' } : undefined;
    return { type: types.SERIAL_MESSAGE, msg: message };
};
export const fetchButtonState = (): ThunkAction<Promise<void>, {}, {}, AnyAction> => {
    // Invoke API
    return async (dispatch: ThunkDispatch<{}, {}, AnyAction>): Promise<void> => {
        return new Promise<void>((resolve) => {
            dispatch(isFetchingButtonState(true));

            g.call('poll_button_state', '')
                .then((data: ButtonState) => {
                    dispatch(setButtonState(data));
                    dispatch(isFetchingButtonState(false));
                })
                .catch((e: Error) => {
                    dispatch(serialError(e));
                });
        });
    };
};

export const closePort = (): ThunkAction<Promise<void>, {}, {}, AnyAction> => {
    // Invoke API
    return async (dispatch: ThunkDispatch<{}, {}, AnyAction>): Promise<void> => {
        return new Promise<void>((resolve) => {
            dispatch(isClosing(true));
            g.call('close_serial_port')
                .then(() => {
                    dispatch(setPortValue(''));
                    dispatch(isClosing(false));
                })
                .catch((e: Error) => dispatch(serialError(e)));
        });
    };
};
export const serialError = (err: Error): ThunkAction<Promise<void>, {}, {}, AnyAction> => {
    // Invoke API
    return async (dispatch: ThunkDispatch<{}, {}, AnyAction>): Promise<void> => {
        return new Promise<void>((resolve) => {
            dispatch(setSerialError(err));
            dispatch(isInputViewer(false));
            dispatch(isStoppingPoll(false));
            dispatch(isStartingPoll(false));
            dispatch(isFlashing(false));
            dispatch(closePort());
        });
    };
};

export const stopPoll = (
    onError?: (err: Error) => void
): ThunkAction<Promise<void>, {}, {}, AnyAction> => {
    // Invoke API
    return async (dispatch: ThunkDispatch<{}, {}, AnyAction>): Promise<void> => {
        return new Promise<void>((resolve) => {
            dispatch(isStoppingPoll(true));
            g.call('stop_button_poll')
                .then(() => {
                    dispatch(setButtonState(defaultButtonState));
                    dispatch(isStoppingPoll(false));
                })
                .catch((e: Error) => {
                    onError && onError(e);
                    dispatch(serialError(e));
                });
        });
    };
};

export const startPoll = (
    type: protos.Config.ControllerType,
    numButtons: number,
    variant?: protos.Config.ControllerVariant,
    onError?: (err: Error) => void
): ThunkAction<Promise<void>, {}, {}, AnyAction> => {
    // Invoke API
    return async (dispatch: ThunkDispatch<{}, {}, AnyAction>): Promise<void> => {
        return new Promise<void>((resolve) => {
            dispatch(isStartingPoll(true));
            g.call('start_button_poll', { type, variant, numButtons })
                .then(() => {
                    dispatch(isStartingPoll(false));
                })
                .catch((e: Error) => {
                    onError && onError(e);
                    dispatch(serialError(e));
                });
        });
    };
};

export const flashFirmware = (file: string): ThunkAction<Promise<void>, {}, {}, AnyAction> => {
    // Invoke API
    return async (dispatch: ThunkDispatch<{}, {}, AnyAction>): Promise<void> => {
        return new Promise<void>((resolve) => {
            dispatch(isFlashing(true));
            g.call('flash_firmware_file', { file })
                .then(() => {
                    dispatch(setSerialMessage('successfully flashed firmware', 'success'));
                    dispatch(isFlashing(false));
                })
                .catch((e: Error) => dispatch(serialError(e)));
        });
    };
};

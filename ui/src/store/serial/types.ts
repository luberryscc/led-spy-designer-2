import { Color } from '@material-ui/lab/Alert';

export const SET_PORTS = 'SET_PORTS';
export const SET_PORT = 'SET_PORT';
export const SET_FETCHING_PORTS = 'SET_FETCHING_PORTS';
export const SET_FETCHING_BUTTON_STATE = 'SET_FETCHING_BUTTON_STATE';
export const SET_SETTING_PORT = 'SET_SETTING_PORT';
export const SET_BUTTON_STATE = 'SET_BUTTON_STATE';
export const SERIAL_CLOSING = 'SERIAL_CLOSING';
export const SERIAL_ERROR = 'SERIAL_ERROR';
export const SERIAL_MESSAGE = 'SERIAL_MESSAGE';
export const SET_STOPPING_POLL = 'SET_STOPPING_POLL';
export const SET_STARTING_POLL = 'SET_STARTING_POLL';
export const FLASH_FIRMWARE = 'FLASH_FIRMWARE';
export const SERIAL_FLASHING = 'SERIAL_FLASHING';

export type Message = {
    severity: Color;
    msg: string;
};

import { isNil, isNull, unset } from 'lodash';
import { CombinedState, combineReducers, AnyAction, Reducer } from 'redux';
import {
    Action,
    SetPortAction,
    SetPortSetting,
    SetPortsFetching,
    SetPortsAction,
    SetButtonStateAction,
    SerialError,
    SerialClosing,
    SetButtonStateFetching,
    SetStopPollAction,
    SetStartPollAction,
    SerialFlashing,
    SerialMessage,
} from './actions';
import * as types from './types';

export interface SerialPorts {
    isFetching: boolean;
    ports?: string | string[];
}
export interface SerialPort {
    isSetting: boolean;
    currentPort?: string | string[];
    lastErr?: Error;
    lastMessage?: types.Message;
    isClosing: boolean;
    flashing: boolean;
}

export interface ButtonState {
    isFetching?: boolean;
    buttons: boolean[];
    dpadSwitch: boolean;
    profile: number;
    lSX: number;
    lSY: number;
    isStoppingPoll: boolean;
    isStartingPoll: boolean;
}

export interface State {
    ports: SerialPorts;
    currentPort: SerialPort;
    buttonState: ButtonState;
}

const defaultPort: SerialPort = { isSetting: false, isClosing: false, flashing: false };
const defaultPorts: SerialPorts = { isFetching: false };
export const defaultButtonState: ButtonState = {
    isFetching: false,
    isStoppingPoll: false,
    buttons: [],
    dpadSwitch: false,
    profile: 0,
    isStartingPoll: false,
    lSX: 0,
    lSY: 0,
};

const currentPort = (state: SerialPort = defaultPort, action: Action): SerialPort => {
    switch (action.type) {
        case types.SET_PORT: {
            const a = action as SetPortAction;
            return { ...state, currentPort: a.currentPort };
        }
        case types.SET_SETTING_PORT: {
            const a = action as SetPortSetting;
            return { ...state, isSetting: a.isSetting };
        }
        case types.SERIAL_ERROR: {
            const a = action as SerialError;
            return { ...state, lastErr: a.err };
        }
        case types.SERIAL_MESSAGE: {
            const a = action as SerialMessage;
            return { ...state, lastMessage: a.msg };
        }
        case types.SERIAL_FLASHING: {
            const a = action as SerialFlashing;
            return { ...state, flashing: a.flashing };
        }
        case types.SERIAL_CLOSING: {
            const a = action as SerialClosing;
            return { ...state, isClosing: a.closing };
        }
        default:
            return state;
    }
};
const ports = (state: SerialPorts = defaultPorts, action: Action): SerialPorts => {
    switch (action.type) {
        case types.SET_PORTS: {
            const a = action as SetPortsAction;
            return { ...state, ports: a.ports };
        }
        case types.SET_FETCHING_PORTS: {
            const a = action as SetPortsFetching;
            return { ...state, isFetching: a.isFetching };
        }
        default:
            return state;
    }
};

const buttonState = (state: ButtonState = defaultButtonState, action: Action): ButtonState => {
    switch (action.type) {
        case types.SET_BUTTON_STATE: {
            const a = action as SetButtonStateAction;
            return {
                ...state,
                buttons: a.buttons,
                dpadSwitch: a.dpadSwitch,
                profile: a.profile,
                lSX: a.lSX,
                lSY: a.lSY,
            };
        }
        case types.SET_FETCHING_BUTTON_STATE: {
            const a = action as SetButtonStateFetching;
            return { ...state, isFetching: a.isFetching };
        }
        case types.SET_STOPPING_POLL: {
            const a = action as SetStopPollAction;
            return { ...state, isStoppingPoll: a.stopping };
        }
        case types.SET_STARTING_POLL: {
            const a = action as SetStartPollAction;
            return { ...state, isStartingPoll: a.starting };
        }
        default:
            return state;
    }
};

const serial: Reducer<CombinedState<State>, AnyAction> = combineReducers<State>({
    ports,
    currentPort,
    buttonState,
});
export default serial;

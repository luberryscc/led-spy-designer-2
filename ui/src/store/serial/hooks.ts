import { RootState } from '../rootTypes';
import { useSelector, useDispatch } from 'react-redux';
import { Dispatch } from 'redux';
import { get, concat } from 'lodash';
import * as actions from './actions';
import { Message } from './types';

export const useCurrentPort = () =>
    useSelector((state: RootState): string => {
        return concat([], get(state.serial.currentPort, 'currentPort', ''))[0];
    });
export const useCurrentPortSetting = () =>
    useSelector((state: RootState): boolean => {
        return get(state.serial.currentPort, 'isSetting', false);
    });

export const usePorts = () =>
    useSelector((state: RootState): string[] => {
        return concat([], get(state.serial.ports, 'ports', [] as string[]));
    });
export const usePortsFetching = () =>
    useSelector((state: RootState): boolean => {
        return concat([], get(state.serial.ports, 'isFetching', false))[0];
    });
export const useFlashing = () =>
    useSelector((state: RootState): boolean => {
        return get(state.serial.currentPort, 'flashing', false);
    });

export const useButtonStateFetching = () =>
    useSelector((state: RootState): boolean => {
        return concat([], get(state.serial.buttonState, 'isFetching', false))[0];
    });
export const useStartingPoll = () =>
    useSelector((state: RootState): boolean => {
        return concat([], get(state.serial.buttonState, 'isStarting', false))[0];
    });

export const useStoppingPoll = () =>
    useSelector((state: RootState): boolean => {
        return concat([], get(state.serial.buttonState, 'isStopping', false))[0];
    });
export const useButtonState = (btnId: number) =>
    useSelector((state: RootState): boolean => {
        return state.serial.buttonState.buttons[btnId];
    });
export const useButtonStates = () =>
    useSelector((state: RootState): boolean[] => {
        return state.serial.buttonState.buttons;
    });
export const useCurrentSwitchProfileId = () =>
    useSelector((state: RootState): number => {
        return state.serial.buttonState.profile;
    });

export const useClosingPort = () =>
    useSelector((state: RootState): boolean => {
        // TODO: use state
        return get(state.serial.currentPort, 'isClosing', false);
    });

export const useSerialError = () =>
    useSelector((state: RootState): Error | undefined => get(state, 'serial.currentPort.lastErr'));

export const useSerialMessage = () =>
    useSelector((state: RootState): Message | undefined =>
        get(state, 'serial.currentPort.lastMessage')
    );

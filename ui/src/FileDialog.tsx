import {
    Button,
    ButtonGroup,
    Dialog,
    DialogActions,
    DialogContent,
    DialogTitle,
    Input,
    Checkbox,
    FormControlLabel,
} from '@material-ui/core';
import React from 'react';
import FolderOpen from '@material-ui/icons/FolderOpen';
import g from 'guark';
export interface Props {
    onSubmit?: (file: string) => void;
    onCancel?: () => void;
    onClose?: () => void;
    open: boolean;
    title: React.ReactNode;
    accept?: string;
    submitText: string;
    isSave?: boolean;
    description?: React.ReactNode;
}
export const FileDialog: React.FC<Props> = ({
    onSubmit,
    onCancel,
    open,
    accept,
    submitText,
    title,
    onClose,
    description,
    isSave,
}) => {
    const handleClose = () => {
        onClose && onClose();
    };
    const handleCancel = () => {
        onCancel && onCancel();
    };
    const [file, setFile] = React.useState('');
    const [filterEnabled, setFilterEnabled] = React.useState(true);
    const handleSubmit = () => {
        onSubmit && onSubmit(file);
    };
    const handleCheck = (event: React.ChangeEvent<HTMLInputElement>) =>
        setFilterEnabled(event.target.checked);
    const filter = () => {
        if (!filterEnabled) {
            return {};
        }
        return {
            file_type_filter_desc: accept,
            file_type_filter: accept,
        };
    };

    return (
        <Dialog open={open} onClose={handleClose}>
            <DialogTitle>{title}</DialogTitle>
            {description && <DialogContent>{description}</DialogContent>}
            <DialogContent style={{ overflow: 'hidden' }}>
                <ButtonGroup>
                    <Button
                        startIcon={<FolderOpen />}
                        onClick={() =>
                            g
                                .call('dialog.file', {
                                    title,
                                    ...filter(),
                                    operation: isSave ? 'save' : 'load',
                                })
                                .then((res: any) => {
                                    setFile(res);
                                })
                        }
                    />
                    <Input disabled value={file} placeholder={'select a file'} />
                </ButtonGroup>

                {!isSave && (
                    <FormControlLabel
                        style={{ width: '100%' }}
                        label="Only show .lspy files"
                        control={<Checkbox checked={filterEnabled} onChange={handleCheck} />}
                    />
                )}
            </DialogContent>
            <DialogActions>
                <Button onClick={handleCancel} color="primary">
                    Cancel
                </Button>
                <Button onClick={handleSubmit} color="primary" autoFocus>
                    {submitText}
                </Button>
            </DialogActions>
        </Dialog>
    );
};

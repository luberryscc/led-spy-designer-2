import React, { Dispatch, Fragment } from 'react';
import clsx from 'clsx';
import { makeStyles, useTheme, Theme, createStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import CssBaseline from '@material-ui/core/CssBaseline';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import {
    useBrighnessRange,
    useControllerType,
    useCurrentAnimation,
    useCurrentBreatheInterval,
    useDrawerOpen,
    useMaxProfiles,
} from './store/config/hooks';
import {
    isDrawerOpen,
    setBrightnessRange,
    setCurrentBreatheInterval,
} from './store/config/actions';
import { useDispatch } from 'react-redux';
import { Slider } from '@material-ui/core';
import { isArray } from 'lodash';
import ControllerType from './ControllerType';
import * as protos from 'led-spy-protos/config';
import ProfileSelect from './ProfileSelect';
import ProfileTypeSelect from './ProfileTypeSelect';
import { breathInterval } from './store/config/types';
import AnimationSelect from './AnimationSelect';
import { SaveLoad } from './SaveLoad';
import { supportsVariant } from './store/config/helpers';
import ControllerVariant from './ControllerVariant';

export interface Props {}
export const drawerWidth = 240;
const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            display: 'flex',
        },
        drawer: {
            width: drawerWidth,
            flexShrink: 0,
        },
        drawerPaper: {
            width: drawerWidth,
        },
        drawerHeader: {
            display: 'flex',
            alignItems: 'center',
            padding: theme.spacing(0, 1),
            // necessary for content to be below app bar
            ...theme.mixins.toolbar,
            justifyContent: 'flex-end',
        },
        content: {
            flexGrow: 1,
            padding: theme.spacing(3),
            transition: theme.transitions.create('margin', {
                easing: theme.transitions.easing.sharp,
                duration: theme.transitions.duration.leavingScreen,
            }),
            marginLeft: -drawerWidth,
        },
        contentShift: {
            transition: theme.transitions.create('margin', {
                easing: theme.transitions.easing.easeOut,
                duration: theme.transitions.duration.enteringScreen,
            }),
            marginLeft: 0,
        },
        slider: {
            width: 200,
        },
    })
);

export const Sidebar: React.FC<Props> = () => {
    const dispatch: Dispatch<any> = useDispatch();
    const theme = useTheme();
    const classes = useStyles();
    const open = useDrawerOpen();
    const maxProfiles = useMaxProfiles();
    const handleDrawerClose = () => {
        dispatch(isDrawerOpen(false));
    };
    const controllerType = useControllerType();
    const hasVariant = supportsVariant(controllerType);
    const [minBright, currBright, maxBright] = useBrighnessRange();
    const animation = useCurrentAnimation();
    const breatheInt = useCurrentBreatheInterval();
    const handleBrightnessChange = (event: any, newValue: number | number[]) => {
        if (!isArray(newValue)) return;
        let [min, curr, max] = newValue;
        min = min != minBright && min != max ? min : minBright;
        max = max != maxBright && max != min ? max : maxBright;
        curr = curr >= min ? (curr <= max ? curr : max) : min;

        dispatch(setBrightnessRange(min, max, curr));
    };
    const handleBreatheChange = (event: any, newValue: number | number[]) => {
        if (isArray(newValue)) return;
        dispatch(setCurrentBreatheInterval(newValue));
    };
    function valuetext(value: number) {
        return `${value * 100}%`;
    }
    function bvaluetext(value: number) {
        return `${((value / breathInterval.max) * 100).toPrecision(4)}%`;
    }
    const bmarks = [
        { value: 0, label: 'min' },
        { value: 0.5, label: 'current' },
        { value: 1, label: 'max' },
    ];
    const brmarks = [
        { value: breathInterval.min, label: 'slow' },
        { value: breathInterval.max, label: 'fast' },
    ];

    return (
        <div className={classes.root}>
            <CssBaseline />
            <Drawer
                className={classes.drawer}
                variant="persistent"
                anchor="left"
                open={open}
                classes={{
                    paper: classes.drawerPaper,
                }}
            >
                <div className={classes.drawerHeader}>
                    <IconButton onClick={handleDrawerClose}>
                        {theme.direction === 'ltr' ? <ChevronLeftIcon /> : <ChevronRightIcon />}
                    </IconButton>
                </div>
                <Divider />
                <List>
                    <ListItemText primary="Controller Type" />
                    <ListItem>
                        <ControllerType />
                    </ListItem>
                    {hasVariant && (
                        <Fragment>
                            <ListItemText primary="Controller Variant" />
                            <ListItem>
                                <ControllerVariant />
                            </ListItem>
                        </Fragment>
                    )}
                </List>
                <Divider />
                {maxProfiles > 1 && (
                    <React.Fragment>
                        <List>
                            <ListItem>
                                <ProfileSelect />
                            </ListItem>
                        </List>
                        <Divider />
                    </React.Fragment>
                )}
                <List>
                    <ListItemText primary="Profile Type" />
                    <ListItem>
                        <ProfileTypeSelect />
                    </ListItem>
                </List>
                <Divider />
                <List>
                    <ListItemText primary="Brightness Range" />
                    <ListItem>
                        <Slider
                            className={classes.slider}
                            value={[minBright, currBright, maxBright]}
                            onChange={handleBrightnessChange}
                            step={0.01}
                            marks={bmarks}
                            max={1}
                            min={0}
                            valueLabelDisplay="auto"
                            aria-labelledby="range-slider"
                            getAriaValueText={valuetext}
                            valueLabelFormat={valuetext}
                        />
                    </ListItem>
                </List>
                <Divider />
                <List>
                    <ListItemText primary="Animation" />
                    <ListItem>
                        <AnimationSelect />
                    </ListItem>
                </List>
                <Divider />
                {animation == protos.Config.Profile.Animation.BREATHE && (
                    <List>
                        <ListItemText primary="Breathe Interval" />
                        <ListItem>
                            <Slider
                                className={classes.slider}
                                value={breatheInt}
                                onChange={handleBreatheChange}
                                step={breathInterval.step}
                                marks={brmarks}
                                max={breathInterval.max}
                                min={breathInterval.min}
                                valueLabelDisplay="auto"
                                aria-labelledby="slider"
                                getAriaValueText={bvaluetext}
                                valueLabelFormat={bvaluetext}
                            />
                        </ListItem>
                    </List>
                )}
                <Divider />
                <List>
                    <ListItemText primary="ConfigFiles" />
                    <ListItem>
                        <SaveLoad />
                    </ListItem>
                </List>
                <Divider />
            </Drawer>
            <main
                className={clsx(classes.content, {
                    [classes.contentShift]: open,
                })}
            >
                <div className={classes.drawerHeader} />
            </main>
        </div>
    );
};

import React, { Dispatch } from 'react';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import * as protos from 'led-spy-protos/config';
import { useDispatch } from 'react-redux';
import { useControllerType, useControllerVariant } from './store/config/hooks';
import { setControllerType } from './store/config/actions';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        formControl: {
            margin: theme.spacing(1),
            width: 200,
        },
        selectEmpty: {
            marginTop: theme.spacing(2),
        },
    })
);
const ControllerVariant: React.FC = () => {
    const dispatch: Dispatch<any> = useDispatch();
    const controllerType = useControllerType();
    const controllerVariant = useControllerVariant();
    const handleChange = (e: any) => dispatch(setControllerType(controllerType, e.target.value));
    const types = ['Other', 'Fight Stick', 'Stickless', 'Fight Stick Extra Directions'];
    const classes = useStyles();

    return (
        <div>
            <FormControl className={classes.formControl}>
                <Select
                    labelId="demo-simple-select-helper-label"
                    id="demo-simple-select-helper"
                    value={controllerVariant || protos.Config.ControllerVariant.FIGHTSTICK}
                    onChange={handleChange}
                >
                    {types &&
                        types.map((key: string, tp: protos.Config.ControllerVariant) => (
                            <MenuItem
                                disabled={
                                    tp == protos.Config.ControllerVariant.OTHER ||
                                    (tp == protos.Config.ControllerVariant.FIGHTSTICK_EXD &&
                                        controllerType ==
                                            protos.Config.ControllerType.BBG_NOSTALGIA)
                                }
                                key={tp}
                                value={tp}
                            >
                                {key}
                            </MenuItem>
                        ))}
                </Select>
            </FormControl>
        </div>
    );
};

export default ControllerVariant;

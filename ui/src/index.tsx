import React, { useEffect } from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import { Provider } from 'react-redux';
import store from './store';
import { ErrorBoundary, FallbackProps } from 'react-error-boundary';
import { Alert } from '@material-ui/lab';
const ErrorFallback: React.FC<FallbackProps> = ({ error, resetErrorBoundary }) => (
    <Alert onClose={resetErrorBoundary} variant="outlined" severity="error">
        {error}
    </Alert>
);
ReactDOM.render(
    <ErrorBoundary FallbackComponent={ErrorFallback}>
        <Provider store={store}>
            <App />
        </Provider>
        ,
    </ErrorBoundary>,
    document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();

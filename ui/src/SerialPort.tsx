import React, { Dispatch } from 'react';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import { indexOf } from 'lodash';
import { fetchPorts, setPort } from './store/serial/actions';
import { usePorts, useCurrentPort } from './store/serial/hooks';
import { useDispatch } from 'react-redux';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        formControl: {
            margin: theme.spacing(1),
            minWidth: 120,
        },
        selectEmpty: {
            marginTop: theme.spacing(2),
        },
    })
);
interface Props {
    className?: string;
}
const SerialPort: React.FC<Props & Record<string, any>> = ({ className }) => {
    const dispatch: Dispatch<any> = useDispatch();
    const handleFetch = () => dispatch(fetchPorts());
    const handleChange = (e: any) => dispatch(setPort(e.target.value));
    const ports = usePorts();
    const currentPort = useCurrentPort();
    const classes = useStyles();

    return (
        <div className={className}>
            <FormControl className={classes.formControl}>
                <InputLabel id="demo-simple-select-helper-label">Serial Port</InputLabel>
                <Select
                    labelId="demo-simple-select-helper-label"
                    id="demo-simple-select-helper"
                    value={indexOf(ports, currentPort) >= 0 ? currentPort : ''}
                    onChange={handleChange}
                    onOpen={handleFetch}
                >
                    {ports &&
                        ports.map((port: string) => (
                            <MenuItem key={port} value={port}>
                                {port}
                            </MenuItem>
                        ))}
                </Select>
            </FormControl>
        </div>
    );
};

export default SerialPort;

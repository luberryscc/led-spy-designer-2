package lib

import (
	"io/ioutil"
	"os"
	"strconv"

	"github.com/pkg/errors"

	"github.com/guark/guark/app"
	"github.com/guark/plugins/clipboard"
	"github.com/guark/plugins/dialog"
	"github.com/guark/plugins/notify"
	"gitlab.com/luberryscc/led-spy-designer/lib/funcs/config"
	"gitlab.com/luberryscc/led-spy-designer/lib/funcs/input"
	"gitlab.com/luberryscc/led-spy-designer/lib/funcs/serial"
	"gitlab.com/luberryscc/led-spy-designer/lib/funcs/socketio"
	"gitlab.com/luberryscc/led-spy-designer/lib/hooks"
	proto "gitlab.com/luberryscc/led-spy-protos/go"
	gproto "google.golang.org/protobuf/proto"
)

var sCli = serial.Default

// Exposed functions to guark Javascript api.
var Funcs = app.Funcs{
	//TODO: maybe make these funcs use context
	"write_raw_config": func(ctx app.Context) (interface{}, error) {
		raw, ok := ctx.Params["raw_cfg"]
		if !ok {
			return nil, errors.New("could not find raw_cfg parameter")
		}
		cfgBytes, ok := raw.(string)
		if !ok {
			return nil, errors.New("invalid type for raw_cfg parameter")
		}
		data, err := config.CleanEncodedBytes(cfgBytes)
		if err != nil {
			return nil, err
		}

		if err := sCli.WriteRawConfig(data); err != nil {
			return nil, err
		}
		return nil, nil
	},
	"write_config_file": func(ctx app.Context) (interface{}, error) {
		raw, ok := ctx.Params["raw_cfg"]
		if !ok {
			return nil, errors.New("could not find raw_cfg parameter")
		}
		file, ok := ctx.Params["file"]
		if !ok {
			return nil, errors.New("could not find file parameter")
		}

		fileStr, ok := file.(string)
		if !ok {
			return nil, errors.New("file should be a string")
		}

		cfgBytes, ok := raw.(string)
		if !ok {
			return nil, errors.New("invalid type for raw_cfg parameter")
		}

		data, err := config.CleanEncodedBytes(cfgBytes)
		if err != nil {
			return nil, err
		}
		if err := ioutil.WriteFile(fileStr, data, 0664); err != nil {
			return nil, err
		}

		return nil, nil
	},
	"read_config_file": func(ctx app.Context) (interface{}, error) {
		file, ok := ctx.Params["file"]
		if !ok {
			return nil, errors.New("could not find file parameter")
		}

		fileStr, ok := file.(string)
		if !ok {
			return nil, errors.New("file should be a string")
		}
		cfgBytes, err := ioutil.ReadFile(fileStr)
		if err != nil {
			return nil, err
		}
		cfg := &config.Config{}
		if err := gproto.Unmarshal(cfgBytes, cfg); err != nil {
			return nil, errors.Wrap(err, "could not unmarshal config for migration")
		}
		if err := cfg.ReadMigrate(); err != nil {
			return nil, errors.Wrap(err, "error migrating config file to new format")
		}
		return cfg.Base64Encode()
	},
	"flash_firmware_file": func(ctx app.Context) (interface{}, error) {
		file, ok := ctx.Params["file"]
		if !ok {
			return nil, errors.New("could not find file parameter")
		}

		fileStr, ok := file.(string)
		if !ok {
			return nil, errors.New("file should be a string")
		}
		return nil, sCli.FlashFirmware(fileStr)
	},
	"read_raw_config": func(app.Context) (interface{}, error) {
		return sCli.ReadBase64Config()
	},
	"list_serial_ports": func(app.Context) (interface{}, error) { return sCli.ListPorts() },
	"set_serial_port": func(ctx app.Context) (interface{}, error) {
		port, ok := ctx.Params["port"]
		if !ok {
			return nil, errors.New("could not find port parameter")
		}
		portStr, ok := port.(string)
		if !ok {
			return nil, errors.New("invalid type for port parameter")
		}
		if err := sCli.SetPort(portStr); err != nil {
			return nil, err
		}
		return nil, nil
	},
	"start_button_poll": func(ctx app.Context) (interface{}, error) {
		tp, err := config.GetControllerTypeParam(ctx.Params)
		if err != nil {
			return nil, err
		}
		nb, err := config.GetNumButtonsParam(ctx.Params)
		if err != nil {
			return nil, err
		}
		opts := []input.DecodeOption{
			input.WithControllerType(tp),
			input.WithApp(ctx.App),
			input.WithNumButtons(nb),
		}
		var variant proto.Config_ControllerVariant
		if config.SupportsVariants[tp] {
			variant, err = config.GetControllerVariantParam(ctx.Params)
			// optional param
			if err != nil && !errors.Is(err, config.ErrNotFound) {
				return nil, err
			}
			opts = append(opts, input.WithControllerVariant(variant))

		}
		sCli.PollButtons(serial.DefaultPollInterval, opts...)
		return nil, nil
	},
	"stop_button_poll": func(app.Context) (interface{}, error) {
		sCli.StopButtonPoll()
		return nil, nil
	},
	"get_default_config": func(ctx app.Context) (interface{}, error) {
		tp, err := config.GetControllerTypeParam(ctx.Params)
		if err != nil {
			return nil, err
		}
		var variant proto.Config_ControllerVariant
		if config.SupportsVariants[tp] {
			variant, err = config.GetControllerVariantParam(ctx.Params)
			// optional param
			if err != nil && !errors.Is(err, config.ErrNotFound) {
				return nil, err
			}

		}

		cfg, ok := config.Defaults(tp, variant)
		if !ok {
			return nil, errors.New("no default profile for selected type and variant")
		}
		return cfg.Base64Encode()
	},
	"close_serial_port": func(app.Context) (interface{}, error) {
		sCli.StopButtonPoll()
		return nil, sCli.Close()
	},
	"socketio_host": func(app.Context) (interface{}, error) {
		return socketio.WSAddr(), nil
	},
	"is_dev": func(ctx app.Context) (interface{}, error) {
		return ctx.App.IsDev() || isDev, nil
	},
}
var isDev bool

func init() {
	isDev, _ = strconv.ParseBool(os.Getenv("MIKU_IS_BEST_GIRL"))
}

// App hooks.
var Hooks = app.Hooks{
	"created": hooks.Created,
	"mounted": hooks.Mounted,
}

// App plugins.
var Plugins = app.Plugins{
	"dialog":    &dialog.Plugin{},
	"notify":    &notify.Plugin{},
	"clipboard": &clipboard.Plugin{},
}

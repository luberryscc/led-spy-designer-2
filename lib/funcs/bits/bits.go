package bits

func BitSet(val uint64, bit int) uint64 {
	return val | (uint64(1) << (bit))
}
func BitClear(val uint64, bit int) uint64 { return val & (^(uint64(1) << (bit))) }
func BitWrite(val uint64, bit int, bitVal bool) uint64 {
	if bitVal {
		return BitSet(val, bit)
	}
	return BitClear(val, bit)
}
func BitRead(val uint64, bit int) bool {
	return (((val) >> (bit)) & 0x01) > 0
}

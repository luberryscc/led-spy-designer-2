package serial

import (
	"bufio"
	"bytes"
	"context"
	"fmt"
	"hash/crc32"
	"io/ioutil"
	"path"
	"runtime"
	"strconv"
	"strings"
	"sync"
	"time"

	usbdrivedetector "github.com/deepakjois/gousbdrivedetector"
	"github.com/guark/guark/app"
	jsoniter "github.com/json-iterator/go"
	mpbserial "github.com/mikepb/go-serial"
	cache "github.com/patrickmn/go-cache"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"

	"github.com/simpleiot/simpleiot/respreader"
	"gitlab.com/luberryscc/led-spy-designer/lib/funcs/config"
	"gitlab.com/luberryscc/led-spy-designer/lib/funcs/input"
	"gitlab.com/luberryscc/led-spy-designer/lib/funcs/socketio"
	"go.bug.st/serial"
	"google.golang.org/protobuf/proto"
)

var json = jsoniter.ConfigCompatibleWithStandardLibrary
var Default *Client

func init() {
	Default = New()
}

type Client struct {
	cache *cache.Cache
	// TODO: maybe switch entirely to the c based serial
	rawPort    serial.Port
	serialPort *respreader.ReadWriteCloser
	app        *app.App
	pollTicker *time.Ticker

	currentBytes []byte
	decoder      *input.Decoder

	comPort string
	baud    int
	cancel  context.CancelFunc
	ctx     context.Context
	mutex   sync.Mutex
}

const (
	CMDRd             = "RD"
	CMDWr             = "WR"
	CMDState          = "STATE"
	CMDSep            = ":"
	CMDOK             = "OK"
	CMDReady          = "READY"
	CMDReload         = "RL"
	CMDFail           = "FAIL"
	CMDBtnTest        = "BTNT"
	CMDUpdateFirmware = "FWUPD8"
	CMDFireHose       = "FHOSE"
)

const (
	defaultBAUD         = 115200
	defaultTimeout      = time.Second
	defaultChunkTimeout = time.Millisecond * 50
	DefaultPollInterval = time.Millisecond * 1
	defaultDiskWait     = time.Second * 5
	vendorName          = "LED Spy"
	fwTargetName        = "NEW.uf2"
)

var (
	ErrPortNotFound       = errors.New("serial port not found")
	ErrSerialPortNotSet   = errors.New("serial port not set")
	ErrSerialRead         = errors.New("could not get data from serialport")
	ErrBadServerCMD       = errors.New("got a bad command from the server")
	ErrChecksumMismatch   = errors.New("checksums mismatch")
	ErrNoPortData         = errors.New("no button data, did you start polling")
	ErrInvalidButtonIndex = errors.New("button index out of range")
)

func New() *Client {
	c := &Client{
		baud:  defaultBAUD,
		cache: cache.New(50*time.Millisecond, 1*time.Minute),
	}
	return c
}
func (c *Client) SetApp(a *app.App) {
	c.app = a
}

// ListPorts lists available serial ports to connect to
func (c *Client) ListPorts() ([]string, error) {
	p, _, err := c.listPorts()
	return p, err
}

const dev = "LED Spy 2.0"

func (c *Client) listPorts() ([]string, map[string]bool, error) {
	ports, err := mpbserial.ListPorts()
	if err != nil {
		return nil, nil, err
	}
	var rports []string
	pm := make(map[string]bool)
	for _, p := range ports {
		name := p.Name()
		if pm[name] || !strings.Contains(p.USBProduct(), dev) && !strings.Contains(p.Description(), dev) {
			continue
		}
		rports = append(rports, name)
		pm[name] = true
	}
	return rports, pm, nil
}

func (c *Client) SetPort(port string) error {
	if c.comPort == port && c.serialPort != nil {
		return nil
	}

	_, ports, err := c.listPorts()
	if err != nil {
		return err
	}
	if !ports[port] {
		return ErrPortNotFound
	}

	c.comPort = port
	return nil
}

func (c *Client) openPort() error {
	// close current port since we are changing.
	if c.serialPort != nil {
		c.close()
	}

	var err error
	c.rawPort, err = serial.Open(c.comPort, &serial.Mode{BaudRate: defaultBAUD})
	if err != nil {
		return err
	}
	c.serialPort = respreader.NewReadWriteCloser(c.rawPort, defaultTimeout, defaultChunkTimeout)
	return nil
}
func (c *Client) WriteRawConfig(raw []byte) error {
	return c.writeCMD(CMDWr, raw)
}
func (c *Client) WriteConfig(cfg *config.Config) error {
	if err := cfg.Cleanup(); err != nil {
		return err
	}
	data, err := proto.Marshal(&cfg.Config)
	if err != nil {
		return err
	}
	return c.WriteRawConfig(data)
}
func (c *Client) writeCMD(cStr string, data []byte) error {

	sum := crc32.ChecksumIEEE(data)

	cmd := fmt.Sprintf("%s%s%d%s%d\n", cStr, CMDSep, len(data), CMDSep, sum)
	if err := c.checkSerial(); err != nil {
		return err
	}
	_, err := c.serialPort.Write([]byte(cmd))
	if err != nil {
		return err
	}
	scanner := bufio.NewScanner(c.serialPort)
	if !scanner.Scan() {
		return ErrSerialRead
	}
	if string(scanner.Bytes()) != CMDReady {
		return ErrBadServerCMD
	}
	_, err = c.serialPort.Write(data)
	if err != nil {
		return err
	}
	if !scanner.Scan() {
		return ErrSerialRead
	}
	if string(scanner.Bytes()) != CMDOK {
		return errors.New(string(scanner.Bytes()))
	}
	return nil
}
func (c *Client) checkSerial() error {
	if err := c.openPort(); err != nil {
		return err
	}
	if c.serialPort == nil {
		return ErrSerialPortNotSet
	}
	// HACK due to this bug
	// https://github.com/bugst/go-serial/issues/93
	if runtime.GOOS != "darwin" {
		if err := c.rawPort.ResetInputBuffer(); err != nil {
			return err
		}
		if err := c.rawPort.ResetOutputBuffer(); err != nil {
			return err
		}
	}
	return nil
}
func (c *Client) ReadJSONConfig() ([]byte, error) {
	cfg, err := c.ReadConfig()
	if err != nil {
		return nil, err
	}
	return json.Marshal(cfg)
}
func (c *Client) ReadBase64Config() (string, error) {
	cfg, err := c.ReadConfig()
	if err != nil {
		return "", err
	}
	return cfg.Base64Encode()
}
func (c *Client) ReadRawConfig() ([]byte, error) {
	if err := c.checkSerial(); err != nil {
		return nil, err
	}
	defer c.close()
	if _, err := c.serialPort.Write([]byte(CMDRd + "\n")); err != nil {
		return nil, err
	}

	scanner := bufio.NewScanner(c.serialPort)
	if !scanner.Scan() {
		return nil, ErrSerialRead
	}
	recv := strings.Split(scanner.Text(), CMDSep)
	if len(recv) != 3 {
		return nil, ErrSerialRead
	}
	if recv[0] != CMDWr {
		return nil, ErrBadServerCMD
	}

	crc, err := strconv.ParseUint(recv[2], 10, 32)
	if err != nil {
		return nil, err
	}
	length, err := strconv.ParseUint(recv[1], 10, 16)
	if err != nil {
		return nil, err
	}

	if _, err := c.serialPort.Write([]byte(CMDReady + "\n")); err != nil {
		return nil, err
	}

	reader := bufio.NewReader(c.serialPort)
	var data []byte
	for i := 0; i < int(length); i++ {
		b, err := reader.ReadByte()
		if err != nil {
			return nil, ErrSerialRead
		}
		data = append(data, b)

	}
	scanner.Scan()
	scanner.Scan()

	sum := crc32.ChecksumIEEE(data)
	if uint32(crc) != sum {
		return nil, ErrChecksumMismatch
	}
	return data, nil
}

func (c *Client) ReadConfig() (*config.Config, error) {
	raw, err := c.ReadRawConfig()
	if err != nil {
		return nil, err
	}
	cfg := &config.Config{}
	if err := proto.Unmarshal(raw, &cfg.Config); err != nil {
		return nil, err
	}
	if err := cfg.ReadMigrate(); err != nil {
		return nil, err
	}
	return cfg, nil
}

func (c *Client) PollButtons(interval time.Duration, opts ...input.DecodeOption) {
	// use raw port for speed

	c.close()
	c.mutex.Lock()
	defer c.mutex.Unlock()
	if c.ctx != nil {
		if c.ctx.Err() == nil {
			c.cancel()
		}
	}
	c.ctx, c.cancel = context.WithCancel(context.Background())
	c.decoder = input.New(opts...)
	if c.pollTicker == nil {
		c.pollTicker = time.NewTicker(interval)
	} else {
		c.pollTicker.Reset(interval)
	}
	c.pollButtons(c.ctx)
}

func (c *Client) StopButtonPoll() {
	c.mutex.Lock()
	defer c.mutex.Unlock()
	if c.pollTicker != nil {
		c.pollTicker.Stop()
	}
	if c.ctx != nil && c.ctx.Err() == nil {
		if c.cancel != nil {
			c.cancel()
			c.cancel = nil
		}
	}
}

// TODO: automated fw download also maybe read values and whatnot so i can restore
func (c *Client) FlashFirmware(p string) error {
	fw, err := ioutil.ReadFile(p)
	if err != nil {
		return err
	}
	c.close()
	// handle already in bootloader mode
	tgt, _ := c.findDisk()
	if tgt == "" {
		port, err := serial.Open(c.comPort, &serial.Mode{BaudRate: 1200})
		if err != nil {
			return err
		}
		port.Close()
		time.Sleep(defaultDiskWait)
		tgt, err = c.findDisk()
		if err != nil {
			return err
		}
	}

	if tgt == "" {
		return errors.New("could not find disk")
	}

	tgt = path.Join(tgt, fwTargetName)
	if err := ioutil.WriteFile(tgt, fw, 0644); err != nil {
		return err
	}
	return nil
}

const errKey = "error"

func (c *Client) errorAdd() int {
	if _, ok := c.cache.Get(errKey); !ok {
		c.cache.SetDefault(errKey, 0)
	}
	c.cache.Increment(errKey, 1)
	v, _ := c.cache.Get(errKey)
	vint, _ := v.(int)
	return vint
}

func (c *Client) findDisk() (string, error) {
	drives, err := usbdrivedetector.Detect()
	if err != nil {
		return "", err
	}
	for _, d := range drives {
		log := logrus.WithField("drive", d)
		if runtime.GOOS != "windows" && strings.Contains(d, "LEDSpyUPD8") {
			return d, nil
		}
		inf, err := ioutil.ReadFile(path.Join(d, "INFO_UF2.txt"))
		if err != nil {
			continue
		}
		log = log.WithField("info", inf)
		if len(inf) < 1 {
			log.Debug("info is empty, continuing")
			continue
		}
		if !bytes.Contains(inf, []byte("Board-ID: SAMD21G18A-LEDSpy-v0")) {
			log.Debug("incorrect board type")
			continue
		}
		return d, nil
	}
	return "", nil
}
func split(data []byte, atEOF bool) (advance int, token []byte, err error) {
	if atEOF && len(data) == 0 {
		return 0, nil, nil
	}
	if i := bytes.Index(data, []byte{5, 7, 2}); i >= 0 {
		// We have a full newline-terminated line.
		return i + 3, data[0:i], nil
	}
	// If we're at EOF, we have a final, non-terminated line. Return it.
	if atEOF {
		return len(data), data, nil
	}
	// Request more data.
	return 0, nil, nil
}
func (c *Client) error(msg interface{}) {
	socketio.Error(msg, socketio.WithName("serial"))
}

func (c *Client) pollButtons(ctx context.Context) {
	var wg sync.WaitGroup
	wg.Add(1)
	go func() {
		defer wg.Done()
		defer c.pollTicker.Stop()
		serialport, err := serial.Open(c.comPort, &serial.Mode{BaudRate: 115200})
		if err != nil {
			return
		}
		serialport.SetReadTimeout(defaultChunkTimeout)
		defer serialport.Close()
		serialport.Write([]byte("FHOSE\n"))
		serialport.ResetInputBuffer()
		scanner := bufio.NewScanner(serialport)
		scanner.Split(split)
		for {
			select {
			case <-ctx.Done():
				return
			default:
				if !scanner.Scan() {
					if c.errorAdd() > 10 {
						c.error("failed to fetch input data")
						return
					}
					continue
				}
				c.currentBytes = scanner.Bytes()
			}
		}
	}()
	wg.Add(1)
	go func() {
		defer wg.Done()
		for {
			select {
			case <-ctx.Done():
				return
			case <-c.pollTicker.C:
				next, err := c.decoder.Decode(c.currentBytes)
				if err != nil {
					c.error(errors.Wrapf(err, "could not decode state"))
					continue
				}
				socketio.Event(fmt.Sprint(next.Profile), socketio.WithName("profile"))
				next.Exec()
			}
		}
	}()

	go func() {
		wg.Wait()
		c.mutex.Lock()
		input.Reset(c.app)
		defer c.mutex.Unlock()
		c.ctx = nil
		c.cancel = nil
	}()
}

func (c *Client) TestButton(idx int) error {
	if err := c.checkSerial(); err != nil {
		return err
	}
	defer c.close()
	scanner := bufio.NewScanner(c.serialPort)
	if _, err := c.serialPort.Write([]byte(fmt.Sprintf("%s%s%d\n", CMDBtnTest, CMDSep, idx))); err != nil {
		return err
	}
	if !scanner.Scan() {
		return ErrSerialRead
	}
	if string(scanner.Bytes()) != CMDOK {
		return ErrBadServerCMD
	}
	return nil
}
func (c *Client) Resume(idx int) error {
	if err := c.checkSerial(); err != nil {
		return err
	}
	defer c.close()
	scanner := bufio.NewScanner(c.serialPort)
	if _, err := c.serialPort.Write([]byte(CMDBtnTest + "\n")); err != nil {
		return err
	}
	if !scanner.Scan() {
		return ErrSerialRead
	}
	if string(scanner.Bytes()) != CMDOK {
		return ErrBadServerCMD
	}
	return nil
}
func (c *Client) Reload() error {
	if err := c.checkSerial(); err != nil {
		return err
	}
	defer c.close()
	scanner := bufio.NewScanner(c.serialPort)
	if _, err := c.serialPort.Write([]byte(CMDReload + "\n")); err != nil {
		return err
	}
	if !scanner.Scan() {
		return ErrSerialRead
	}
	if string(scanner.Bytes()) != CMDOK {
		return ErrBadServerCMD
	}
	return nil

}
func (c *Client) close() error {
	if c.serialPort == nil || c.rawPort == nil {
		return nil
	}
	c.StopButtonPoll()
	defer recover()
	defer func() {
		c.serialPort = nil
		c.rawPort = nil
	}()
	return c.rawPort.Close()
}

func (c *Client) Close() error {
	defer func() {
		c.comPort = ""
	}()
	return c.close()
}

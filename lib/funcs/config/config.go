package config

import (
	"encoding/base64"
	"math"

	"github.com/guark/guark/app"
	"github.com/pkg/errors"
	"gitlab.com/luberryscc/led-spy-designer/lib/funcs/bits"
	proto "gitlab.com/luberryscc/led-spy-protos/go"
	gproto "google.golang.org/protobuf/proto"
)

type Config struct {
	proto.Config
}

func (c *Config) ReadMigrate() error {
	c.migrateAnimation()
	return nil
}
func (c *Config) Cleanup() error {
	for _, p := range c.Profiles {
		if p.Animation == proto.Config_Profile_BREATHE {
			continue
		}
		p.BreatheInterval = 0
	}
	return nil
}

func (c *Config) migrateAnimation() {
	for _, p := range c.Profiles {
		if p.Animation != proto.Config_Profile_NIL {
			continue
		}
		if p.BreatheInterval <= 0 {
			p.Animation = proto.Config_Profile_NONE
			continue
		}
		p.Animation = proto.Config_Profile_BREATHE
	}
}

func (c *Config) Base64Encode() (string, error) {
	cfg, err := gproto.Marshal(c)
	if err != nil {
		return "", err
	}
	d := base64.StdEncoding.EncodeToString(cfg)
	return d, nil
}

var defaults map[proto.Config_ControllerType]*Config
var defaultVariants map[proto.Config_ControllerType]map[proto.Config_ControllerVariant]*Config

var SupportsVariants = map[proto.Config_ControllerType]bool{
	proto.Config_GENERIC:       true,
	proto.Config_BBG_NOSTALGIA: true,
}

var initialVariants = map[proto.Config_ControllerType]proto.Config_ControllerVariant{
	proto.Config_BBG_NOSTALGIA: proto.Config_FIGHTSTICK,
	proto.Config_GENERIC:       proto.Config_FIGHTSTICK,
}

func Defaults(tp proto.Config_ControllerType, variant proto.Config_ControllerVariant) (*Config, bool) {
	if SupportsVariants[tp] {
		c, ok := defaultVariants[tp][variant]
		if !ok {
			variant, ok = initialVariants[tp]
			if !ok {
				return c, ok
			}
			c, ok = defaultVariants[tp][variant]
		}
		return c, ok
	}
	c, ok := defaults[tp]
	return c, ok
}

func setColor(color []byte, colors []byte, idx int) {
	id := idx * 4
	for i, c := range color {
		colors[id+i] = c
	}
}

func hbxColors(mappings []byte) (colors []byte, reactColors []byte) {
	colors = make([]byte, 18*4)
	reactColors = make([]byte, 18*4)
	bmp := make(map[int]int)
	for idx, mp := range mappings {
		bmp[int(mp)] = idx
	}
	// RGBA Color
	red := []byte{0xFF, 0x00, 0x00, 0xFF}
	white := []byte{0xFF, 0xFF, 0xFF, 0xFF}
	// Analog LDRU
	// color
	setColor(red, colors, bmp[10])
	setColor(red, colors, bmp[9])
	setColor(red, colors, bmp[11])
	setColor(red, colors, bmp[8])
	// react color
	setColor(white, reactColors, bmp[10])
	setColor(white, reactColors, bmp[9])
	setColor(white, reactColors, bmp[11])
	setColor(white, reactColors, bmp[8])

	//Punch (top)
	// color
	setColor(white, colors, bmp[0])
	setColor(white, colors, bmp[1])
	setColor(white, colors, bmp[2])
	setColor(white, colors, bmp[3])
	// react color
	setColor(red, reactColors, bmp[0])
	setColor(red, reactColors, bmp[1])
	setColor(red, reactColors, bmp[2])
	setColor(red, reactColors, bmp[3])

	//Kick (bottom)
	// color
	setColor(white, colors, bmp[4])
	setColor(white, colors, bmp[5])
	setColor(white, colors, bmp[6])
	setColor(white, colors, bmp[7])
	// react color
	setColor(red, reactColors, bmp[4])
	setColor(red, reactColors, bmp[5])
	setColor(red, reactColors, bmp[6])
	setColor(red, reactColors, bmp[7])

	// Rear Buttons
	// color
	setColor(white, colors, bmp[12])
	setColor(white, colors, bmp[13])
	setColor(white, colors, bmp[14])
	setColor(white, colors, bmp[15])
	// react color
	setColor(red, reactColors, bmp[12])
	setColor(red, reactColors, bmp[13])
	setColor(red, reactColors, bmp[14])
	setColor(red, reactColors, bmp[15])

	// l3,r3
	// color
	setColor(white, colors, bmp[16])
	setColor(white, colors, bmp[17])
	// react color
	setColor(red, reactColors, bmp[16])
	setColor(red, reactColors, bmp[17])

	return
}
func bbgColors(mappings []byte) (colors []byte, reactColors []byte) {
	colors = make([]byte, 19*4)
	reactColors = make([]byte, 19*4)
	bmp := make(map[int]int)
	for idx, mp := range mappings {
		bmp[int(mp)] = idx
	}
	// RGBA Color
	red := []byte{0xFF, 0x00, 0x00, 0xFF}
	white := []byte{0xFF, 0xFF, 0xFF, 0xFF}
	// Analog LDRU
	// color
	setColor(red, colors, bmp[10])
	setColor(red, colors, bmp[9])
	setColor(red, colors, bmp[11])
	setColor(red, colors, bmp[8])
	// react color
	setColor(white, reactColors, bmp[10])
	setColor(white, reactColors, bmp[9])
	setColor(white, reactColors, bmp[11])
	setColor(white, reactColors, bmp[8])

	//Punch (top)
	// color
	setColor(white, colors, bmp[0])
	setColor(white, colors, bmp[1])
	setColor(white, colors, bmp[2])
	setColor(white, colors, bmp[3])
	// react color
	setColor(red, reactColors, bmp[0])
	setColor(red, reactColors, bmp[1])
	setColor(red, reactColors, bmp[2])
	setColor(red, reactColors, bmp[3])

	//Kick (bottom)
	// color
	setColor(white, colors, bmp[4])
	setColor(white, colors, bmp[5])
	setColor(white, colors, bmp[6])
	setColor(white, colors, bmp[7])
	// react color
	setColor(red, reactColors, bmp[4])
	setColor(red, reactColors, bmp[5])
	setColor(red, reactColors, bmp[6])
	setColor(red, reactColors, bmp[7])

	// Rear Buttons
	// color
	setColor(white, colors, bmp[12])
	setColor(white, colors, bmp[13])
	setColor(white, colors, bmp[14])
	setColor(white, colors, bmp[15])
	// react color
	setColor(red, reactColors, bmp[12])
	setColor(red, reactColors, bmp[13])
	setColor(red, reactColors, bmp[14])
	setColor(red, reactColors, bmp[15])

	// l3,r3
	// color
	setColor(white, colors, bmp[16])
	setColor(white, colors, bmp[17])
	// react color
	setColor(red, reactColors, bmp[16])
	setColor(red, reactColors, bmp[17])
	// turbo
	setColor(white, colors, bmp[18])
	setColor(red, reactColors, bmp[18])

	return
}
func genericColors(mappings []byte) (colors []byte, reactColors []byte) {
	colors = make([]byte, 29*4)
	reactColors = make([]byte, 29*4)
	// RGBA Color
	red := []byte{0xFF, 0x00, 0x00, 0xFF}
	white := []byte{0xFF, 0xFF, 0xFF, 0xFF}
	for idx := range mappings {
		setColor(white, colors, idx)
		setColor(red, reactColors, idx)
	}
	setColor(red, colors, 0)
	setColor(white, reactColors, 0)
	setColor(red, colors, 1)
	setColor(white, reactColors, 1)
	setColor(red, colors, 2)
	setColor(white, reactColors, 2)
	setColor(red, colors, 3)
	setColor(white, reactColors, 3)
	return
}
func xuColors(mappings []byte) (colors []byte, reactColors []byte) {
	colors = make([]byte, 21*4)
	reactColors = make([]byte, 21*4)
	bmp := make(map[int]int)
	for idx, mp := range mappings {
		bmp[int(mp)] = idx
	}
	// RGBA Color
	red := []byte{0xFF, 0x00, 0x00, 0xFF}
	white := []byte{0xFF, 0xFF, 0xFF, 0xFF}
	// Analog LDRU
	// color
	setColor(red, colors, bmp[0])
	setColor(red, colors, bmp[1])
	setColor(red, colors, bmp[2])
	setColor(red, colors, bmp[3])
	// react color
	setColor(white, reactColors, bmp[0])
	setColor(white, reactColors, bmp[1])
	setColor(white, reactColors, bmp[2])
	setColor(white, reactColors, bmp[3])

	//Punch (top)
	// color
	setColor(white, colors, bmp[4])
	setColor(white, colors, bmp[5])
	setColor(white, colors, bmp[6])
	setColor(white, colors, bmp[7])
	// react color
	setColor(red, reactColors, bmp[4])
	setColor(red, reactColors, bmp[5])
	setColor(red, reactColors, bmp[6])
	setColor(red, reactColors, bmp[7])

	//Kick (bottom)
	// color
	setColor(white, colors, bmp[8])
	setColor(white, colors, bmp[9])
	setColor(white, colors, bmp[10])
	setColor(white, colors, bmp[11])
	// react color
	setColor(red, reactColors, bmp[8])
	setColor(red, reactColors, bmp[9])
	setColor(red, reactColors, bmp[10])
	setColor(red, reactColors, bmp[11])

	yellow := []byte{0xFF, 0xFF, 0x00, 0xFF}
	// RS l,d,r,u
	// color
	setColor(yellow, colors, bmp[12])
	setColor(yellow, colors, bmp[13])
	setColor(yellow, colors, bmp[14])
	setColor(yellow, colors, bmp[15])
	// react color
	setColor(white, reactColors, bmp[12])
	setColor(white, reactColors, bmp[13])
	setColor(white, reactColors, bmp[14])
	setColor(white, reactColors, bmp[15])
	// Rear Buttons
	// color
	setColor(white, colors, bmp[16])
	setColor(white, colors, bmp[17])
	setColor(white, colors, bmp[18])
	setColor(white, colors, bmp[19])
	// hotkey
	setColor(white, colors, bmp[20])
	// react color
	setColor(red, reactColors, bmp[16])
	setColor(red, reactColors, bmp[17])
	setColor(red, reactColors, bmp[18])
	setColor(red, reactColors, bmp[19])
	// Hotkey
	setColor(red, reactColors, bmp[20])

	// l3,r3
	// color
	setColor(white, colors, bmp[16])
	setColor(white, colors, bmp[17])
	// react color
	setColor(red, reactColors, bmp[16])
	setColor(red, reactColors, bmp[17])

	return
}
func sbxColors(mappings []byte) (colors []byte, reactColors []byte) {
	colors = make([]byte, 23*4)
	reactColors = make([]byte, 23*4)
	bmp := make(map[int]int)
	for idx, mp := range mappings {
		bmp[int(mp)] = idx
	}
	// RGBA Color
	red := []byte{0xFF, 0x00, 0x00, 0xFF}
	white := []byte{0xFF, 0xFF, 0xFF, 0xFF}
	yellow := []byte{0xFF, 0xFF, 0x00, 0xFF}
	// Analog LDRU
	// color
	setColor(red, colors, bmp[0])
	setColor(red, colors, bmp[1])
	setColor(red, colors, bmp[2])
	setColor(red, colors, bmp[3])
	// react color
	setColor(white, reactColors, bmp[0])
	setColor(white, reactColors, bmp[1])
	setColor(white, reactColors, bmp[2])
	setColor(white, reactColors, bmp[3])

	//Punch (top)
	// color
	setColor(white, colors, bmp[4])
	setColor(white, colors, bmp[5])
	setColor(white, colors, bmp[6])
	setColor(white, colors, bmp[7])
	// react color
	setColor(red, reactColors, bmp[4])
	setColor(red, reactColors, bmp[5])
	setColor(red, reactColors, bmp[6])
	setColor(red, reactColors, bmp[7])

	//Kick (bottom)
	// color
	setColor(white, colors, bmp[8])
	setColor(white, colors, bmp[9])
	setColor(white, colors, bmp[10])
	setColor(white, colors, bmp[11])
	// react color
	setColor(red, reactColors, bmp[8])
	setColor(red, reactColors, bmp[9])
	setColor(red, reactColors, bmp[10])
	setColor(red, reactColors, bmp[11])

	//C Stick URLDM
	// color
	setColor(yellow, colors, bmp[12])
	setColor(yellow, colors, bmp[13])
	setColor(yellow, colors, bmp[14])
	setColor(yellow, colors, bmp[15])
	setColor(white, colors, bmp[16])
	// react color
	setColor(white, reactColors, bmp[12])
	setColor(white, reactColors, bmp[13])
	setColor(white, reactColors, bmp[14])
	setColor(white, reactColors, bmp[15])
	setColor(red, reactColors, bmp[16])

	// Modifiers and DPAD TLT,DPL,DPU,DPD,DPR
	// color
	setColor(white, colors, bmp[17])
	setColor(white, colors, bmp[18])
	setColor(white, colors, bmp[19])
	setColor(white, colors, bmp[20])
	setColor(white, colors, bmp[21])
	// react color
	setColor(red, reactColors, bmp[17])
	setColor(red, reactColors, bmp[18])
	setColor(red, reactColors, bmp[19])
	setColor(red, reactColors, bmp[20])
	setColor(red, reactColors, bmp[21])

	// start
	// color
	setColor(white, colors, bmp[22])
	// react color
	setColor(red, reactColors, bmp[22])
	return
}
func init() {
	sbx := &Config{
		Config: proto.Config{
			ControllerType: proto.Config_SMASH_BOX,
			Brightness:     0.5,
			MaxBrightness:  1,
			MinBrightness:  0.0,
			Mappings: []byte{
				16, 15, 13, 8, 9, 10, 11, 7, 6, 5, 4, 12, 14, 21, 20, 18, 19, 2, 3, 1, 0, 17, 22,
			},
			NumProfiles:    3,
			SwitchProfiles: []byte{0, 1, 2},
			Profiles: []*proto.Config_Profile{
				{
					Type:            proto.Config_STATIC,
					Animation:       proto.Config_Profile_NONE,
					BreatheInterval: 0.00,
				},
				{
					Type:            proto.Config_REACTIVE,
					Animation:       proto.Config_Profile_NONE,
					BreatheInterval: 0.000,
				},
				{
					Type:            proto.Config_STATIC,
					Animation:       proto.Config_Profile_NONE,
					BreatheInterval: 0.0,
				},
			},
		},
	}
	sbc, sbcr := sbxColors(sbx.Mappings)
	for _, p := range sbx.Profiles {
		switch p.Type {
		case proto.Config_STATIC:

			p.ProfileSettings = &proto.Config_Profile_Pstatic{
				Pstatic: &proto.Config_Profile_Static{
					Colors: sbc[:],
				},
			}
		case proto.Config_REACTIVE:
			p.ProfileSettings = &proto.Config_Profile_Preactive{
				Preactive: &proto.Config_Profile_Reactive{
					Colors:      sbc[:],
					ReactColors: sbcr[:],
				},
			}
		}

	}

	hbx := &Config{
		Config: proto.Config{
			ControllerType: proto.Config_HIT_BOX,
			Brightness:     0.25,
			MaxBrightness:  1,
			MinBrightness:  0.0,
			Mappings: []byte{
				10, 9, 11, 8, 0, 4, 1, 5, 2, 6, 3, 7, 14, 12, 15, 13, 17, 16,
			},
			NumProfiles:    1,
			SwitchProfiles: []byte{0},
			Profiles: []*proto.Config_Profile{
				{
					Type:            proto.Config_REACTIVE,
					BreatheInterval: 0,
					Animation:       proto.Config_Profile_NONE,
					DisabledButtons: uint32(bits.BitSet(bits.BitSet(uint64(0), 17), 16)),
				},
			},
		},
	}
	hbc, hbcr := hbxColors(hbx.Mappings)
	for _, p := range hbx.Profiles {
		switch p.Type {
		case proto.Config_STATIC:

			p.ProfileSettings = &proto.Config_Profile_Pstatic{
				Pstatic: &proto.Config_Profile_Static{
					Colors: hbc[:],
				},
			}
		case proto.Config_REACTIVE:
			p.ProfileSettings = &proto.Config_Profile_Preactive{
				Preactive: &proto.Config_Profile_Reactive{
					Colors:      hbc[:],
					ReactColors: hbcr[:],
				},
			}
		}

	}
	bbg := &Config{
		Config: proto.Config{
			ControllerType: proto.Config_BBG_NOSTALGIA,
			Variant:        proto.Config_STICKLESS,
			Brightness:     0.25,
			MaxBrightness:  1,
			MinBrightness:  0.0,
			Mappings: []byte{
				10, 9, 11, 8, 0, 4, 1, 5, 2, 6, 3, 7, 14, 12, 15, 13, 17, 16, 18,
			},
			NumProfiles:    1,
			SwitchProfiles: []byte{0},
			Profiles: []*proto.Config_Profile{
				{
					Type:            proto.Config_REACTIVE,
					BreatheInterval: 0,
					Animation:       proto.Config_Profile_NONE,
					DisabledButtons: uint32(bits.BitSet(uint64(0), 18)),
				},
			},
		},
	}
	bbgfs := &Config{
		Config: proto.Config{
			ControllerType: proto.Config_BBG_NOSTALGIA,
			Variant:        proto.Config_FIGHTSTICK,
			Brightness:     0.25,
			MaxBrightness:  1,
			MinBrightness:  0.0,
			Mappings: []byte{
				10, 9, 11, 8, 0, 4, 1, 5, 2, 6, 3, 7, 14, 12, 15, 13, 17, 16, 18,
			},
			NumProfiles:    1,
			SwitchProfiles: []byte{0},
			Profiles: []*proto.Config_Profile{
				{
					Type:            proto.Config_REACTIVE,
					BreatheInterval: 0,
					DisabledButtons: uint32(bits.BitSet(uint64(0), 18)),
				},
			},
		},
	}
	bbc, bbcr := bbgColors(bbg.Mappings)
	red := []byte{0xFF, 0x00, 0x00, 0xFF}
	white := []byte{0xFF, 0xFF, 0xFF, 0xFF}
	bbc = append(bbc, white...)
	bbcr = append(bbcr, red...)

	for _, p := range bbg.Profiles {
		switch p.Type {
		case proto.Config_STATIC:

			p.ProfileSettings = &proto.Config_Profile_Pstatic{
				Pstatic: &proto.Config_Profile_Static{
					Colors: bbc[:],
				},
			}
		case proto.Config_REACTIVE:
			p.ProfileSettings = &proto.Config_Profile_Preactive{
				Preactive: &proto.Config_Profile_Reactive{
					Colors:      bbc[:],
					ReactColors: bbcr[:],
				},
			}
		}

	}
	black := make([]byte, 4)
	black[3] = 0xFF
	bbcfs, bbcrfs := bbgColors(bbg.Mappings)
	setColor(black, bbcfs, 0)
	setColor(black, bbcfs, 1)
	setColor(black, bbcfs, 2)
	setColor(black, bbcfs, 3)
	bbcfs = append(bbcfs, white...)
	bbcrfs = append(bbcrfs, red...)
	for _, p := range bbgfs.Profiles {
		switch p.Type {
		case proto.Config_STATIC:

			p.ProfileSettings = &proto.Config_Profile_Pstatic{
				Pstatic: &proto.Config_Profile_Static{
					Colors: bbcfs[:],
				},
			}
		case proto.Config_REACTIVE:
			p.ProfileSettings = &proto.Config_Profile_Preactive{
				Preactive: &proto.Config_Profile_Reactive{
					Colors:      bbcfs[:],
					ReactColors: bbcrfs[:],
				},
			}
		}

	}
	xu := &Config{
		Config: proto.Config{
			ControllerType: proto.Config_SMASH_BOX,
			Brightness:     0.5,
			MaxBrightness:  1,
			MinBrightness:  0.0,
			Mappings: []byte{
				// TODO: figure out default led mappings for xu
				20, 19, 17, 18, 10, 9, 8, 7, 6, 5, 4, 3, 11, 2, 12, 1, 0, 15, 16, 14, 13,
			},
			NumProfiles:    3,
			SwitchProfiles: []byte{0, 1, 2},
			Profiles: []*proto.Config_Profile{
				{
					Type:            proto.Config_STATIC,
					BreatheInterval: 0.001,
				},
				{
					Type:            proto.Config_REACTIVE,
					BreatheInterval: 0.001,
				},
				{
					Type:            proto.Config_STATIC,
					BreatheInterval: 0.0,
				},
			},
		},
	}
	xuc, xucr := xuColors(xu.Mappings)
	for _, p := range xu.Profiles {
		switch p.Type {
		case proto.Config_STATIC:

			p.ProfileSettings = &proto.Config_Profile_Pstatic{
				Pstatic: &proto.Config_Profile_Static{
					Colors: xuc[:],
				},
			}
		case proto.Config_REACTIVE:
			p.ProfileSettings = &proto.Config_Profile_Preactive{
				Preactive: &proto.Config_Profile_Reactive{
					Colors:      xuc[:],
					ReactColors: xucr[:],
				},
			}
		}

	}
	gen := &Config{
		Config: proto.Config{
			ControllerType: proto.Config_GENERIC,
			Variant:        proto.Config_STICKLESS,
			Brightness:     0.5,
			MaxBrightness:  1,
			MinBrightness:  0.0,
			Mappings: []byte{
				// TODO: figure out default led mappings for hit box
				10, 9, 11, 8, 0, 4, 1, 5, 2, 6, 3, 7, 14, 12, 15, 13, 17, 16, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28,
			},
			NumProfiles:    3,
			SwitchProfiles: []byte{0, 1, 2},
			Profiles: []*proto.Config_Profile{
				{
					Type:            proto.Config_STATIC,
					BreatheInterval: 0.001,
				},
				{
					Type:            proto.Config_REACTIVE,
					BreatheInterval: 0.001,
				},
				{
					Type:            proto.Config_STATIC,
					BreatheInterval: 0.0,
				},
			},
		},
	}
	genc, gencr := genericColors(gen.Mappings)
	for _, p := range gen.Profiles {
		switch p.Type {
		case proto.Config_STATIC:

			p.ProfileSettings = &proto.Config_Profile_Pstatic{
				Pstatic: &proto.Config_Profile_Static{
					Colors: genc[:],
				},
			}
		case proto.Config_REACTIVE:
			p.ProfileSettings = &proto.Config_Profile_Preactive{
				Preactive: &proto.Config_Profile_Reactive{
					Colors:      genc[:],
					ReactColors: gencr[:],
				},
			}
		}

	}
	genfs := &Config{
		Config: proto.Config{
			ControllerType: proto.Config_GENERIC,
			Variant:        proto.Config_FIGHTSTICK,
			Brightness:     0.5,
			MaxBrightness:  1,
			MinBrightness:  0.0,
			Mappings: []byte{
				// TODO: figure out default led mappings for hit box
				10, 9, 11, 8, 0, 4, 1, 5, 2, 6, 3, 7, 14, 12, 15, 13, 17, 16, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28,
			},
			NumProfiles:    3,
			SwitchProfiles: []byte{0, 1, 2},
			Profiles: []*proto.Config_Profile{
				{
					Type:            proto.Config_STATIC,
					Animation:       proto.Config_Profile_BREATHE,
					BreatheInterval: 0.001,
				},
				{
					Type:            proto.Config_REACTIVE,
					Animation:       proto.Config_Profile_BREATHE,
					BreatheInterval: 0.001,
				},
				{
					Type:            proto.Config_STATIC,
					Animation:       proto.Config_Profile_NONE,
					BreatheInterval: 0.0,
				},
			},
		},
	}
	genfsex := &Config{
		Config: proto.Config{
			ControllerType: proto.Config_GENERIC,
			Variant:        proto.Config_FIGHTSTICK_EXD,
			Brightness:     0.5,
			MaxBrightness:  1,
			MinBrightness:  0.0,
			Mappings: []byte{
				// TODO: figure out default led mappings for hit box
				10, 9, 11, 8, 0, 4, 1, 5, 2, 6, 3, 7, 14, 12, 15, 13, 17, 16, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28,
			},
			NumProfiles:    3,
			SwitchProfiles: []byte{0, 1, 2},
			Profiles: []*proto.Config_Profile{
				{
					Type:            proto.Config_STATIC,
					Animation:       proto.Config_Profile_BREATHE,
					BreatheInterval: 0.001,
				},
				{
					Type:            proto.Config_REACTIVE,
					Animation:       proto.Config_Profile_BREATHE,
					BreatheInterval: 0.001,
				},
				{
					Type:            proto.Config_STATIC,
					BreatheInterval: 0.0,
					Animation:       proto.Config_Profile_NONE,
				},
			},
		},
	}
	gencfs, gencrfs := genericColors(genfs.Mappings)
	setColor(black, gencfs, 0)
	setColor(black, gencfs, 1)
	setColor(black, gencfs, 2)
	setColor(black, gencfs, 3)
	for _, p := range genfs.Profiles {
		switch p.Type {
		case proto.Config_STATIC:

			p.ProfileSettings = &proto.Config_Profile_Pstatic{
				Pstatic: &proto.Config_Profile_Static{
					Colors: gencfs[:],
				},
			}
		case proto.Config_REACTIVE:
			p.ProfileSettings = &proto.Config_Profile_Preactive{
				Preactive: &proto.Config_Profile_Reactive{
					Colors:      gencfs[:],
					ReactColors: gencrfs[:],
				},
			}
		}

	}
	for _, p := range genfsex.Profiles {
		switch p.Type {
		case proto.Config_STATIC:

			p.ProfileSettings = &proto.Config_Profile_Pstatic{
				Pstatic: &proto.Config_Profile_Static{
					Colors: gencfs[:],
				},
			}
		case proto.Config_REACTIVE:
			p.ProfileSettings = &proto.Config_Profile_Preactive{
				Preactive: &proto.Config_Profile_Reactive{
					Colors:      gencfs[:],
					ReactColors: gencrfs[:],
				},
			}
		}

	}

	defaults = map[proto.Config_ControllerType]*Config{
		proto.Config_SMASH_BOX: sbx,
		proto.Config_HIT_BOX:   hbx,
		proto.Config_CROSSUP:   xu,
	}
	defaultVariants = map[proto.Config_ControllerType]map[proto.Config_ControllerVariant]*Config{
		proto.Config_BBG_NOSTALGIA: {
			proto.Config_STICKLESS:  bbg,
			proto.Config_FIGHTSTICK: bbgfs,
		},
		proto.Config_GENERIC: {
			proto.Config_STICKLESS:      gen,
			proto.Config_FIGHTSTICK:     genfs,
			proto.Config_FIGHTSTICK_EXD: genfsex,
		},
	}
	// Setup disabled mappings for anything between profile and max mappings
	for _, prof := range defaults {
		l := len(prof.Mappings)
		for i := 0; i < 29-l; i++ {
			prof.Mappings = append(prof.Mappings, math.MaxUint8)
		}
	}
	for _, tp := range defaultVariants {
		// Setup disabled mappings for anything between profile and max mappings
		for _, prof := range tp {
			l := len(prof.Mappings)
			for i := 0; i < 29-l; i++ {
				prof.Mappings = append(prof.Mappings, math.MaxUint8)
			}
		}
	}
}

func GetControllerTypeParam(params app.Params) (proto.Config_ControllerType, error) {
	var tp proto.Config_ControllerType

	tpRaw, ok := params["type"]
	if !ok {
		return tp, errors.Wrap(ErrNotFound, "controller type required")
	}
	ftp, ok := tpRaw.(float64)
	if !ok {
		return tp, errors.Errorf("controller type is not a valid integer %T", tpRaw)
	}
	tp = proto.Config_ControllerType(ftp)

	if _, ok = proto.Config_ControllerType_name[int32(tp)]; !ok {
		// TODO: log here
		tp = proto.Config_GENERIC
	}
	return tp, nil
}

var ErrNotFound = errors.New("not found")

func GetControllerVariantParam(params app.Params) (proto.Config_ControllerVariant, error) {
	var tp proto.Config_ControllerVariant

	tpRaw, ok := params["variant"]
	if !ok {
		return tp, errors.Wrap(ErrNotFound, "controller variant required")
	}
	ftp, ok := tpRaw.(float64)
	if !ok {
		return tp, errors.Errorf("controller variant is not a valid integer %T", tpRaw)
	}
	tp = proto.Config_ControllerVariant(ftp)

	if _, ok = proto.Config_ControllerVariant_name[int32(tp)]; !ok {
		// TODO: log here
		tp = proto.Config_OTHER
	}
	return tp, nil
}
func GetNumButtonsParam(params app.Params) (int, error) {
	var btns int

	btnRaw, ok := params["numButtons"]
	if !ok {
		return btns, errors.Wrap(ErrNotFound, "num buttons required")
	}
	fnb, ok := btnRaw.(float64)
	if !ok {
		return btns, errors.Errorf("num buttons is not a valid integer %T", btnRaw)
	}
	btns = int(fnb)

	return btns, nil
}

func CleanEncodedBytes(encoded string) ([]byte, error) {
	data, err := base64.StdEncoding.DecodeString(encoded)
	if err != nil {
		return nil, err
	}
	return CleanBytes(data)
}
func CleanBytes(data []byte) ([]byte, error) {
	var err error
	cfg := &Config{}
	if err := gproto.Unmarshal(data, cfg); err != nil {
		return nil, errors.Wrap(err, "could not unmarshal config for cleanup")
	}
	if err := cfg.Cleanup(); err != nil {
		return nil, errors.Wrap(err, "error cleaning up config")
	}
	if data, err = gproto.Marshal(cfg); err != nil {
		return nil, errors.Wrap(err, "could not marshal cleaned up config")
	}
	return data, nil
}

package socketio

import (
	"net"
	"net/http"
	"strings"

	socketio "github.com/googollee/go-socket.io"
	"github.com/googollee/go-socket.io/engineio"
	"github.com/googollee/go-socket.io/engineio/transport"
	"github.com/googollee/go-socket.io/engineio/transport/polling"
	"github.com/googollee/go-socket.io/engineio/transport/websocket"
	jsoniter "github.com/json-iterator/go"
	"github.com/sirupsen/logrus"
)

var json = jsoniter.ConfigCompatibleWithStandardLibrary

var server *socketio.Server

// Easier to get running with CORS. Thanks for help @Vindexus and @erkie
var allowOriginFunc = func(r *http.Request) bool {
	return true
}

type Evt string

const (
	ErrorEvt   Evt = "error"
	GenericEvt Evt = "event"
	StatusEvt  Evt = "status"
)

type SockOpts struct {
	namespace string
	room      string
	name      string
}
type SockOpt func(opts *SockOpts)

func WithName(name string) SockOpt {
	return func(so *SockOpts) {
		so.name = name
	}
}
func WithRoom(room string) SockOpt {
	return func(so *SockOpts) {
		so.room = room
	}
}
func WithNamespace(ns string) SockOpt {
	return func(so *SockOpts) {
		so.namespace = ns
	}
}
func sockOpts(opts []SockOpt) *SockOpts {
	so := &SockOpts{}
	for _, opt := range opts {
		opt(so)
	}
	return so
}

func Error(msg interface{}, opts ...SockOpt) {
	event(msg, ErrorEvt, opts)
}

func (so *SockOpts) evtPath(evt Evt) string {
	var b strings.Builder
	b.WriteString(string(evt))
	if so.name != "" {
		b.WriteRune(':')
		b.WriteString(so.name)
	}
	return b.String()
}

func Event(msg interface{}, opts ...SockOpt) {
	event(msg, GenericEvt, opts)
}
func Status(msg interface{}, opts ...SockOpt) {
	event(msg, StatusEvt, opts)
}

func event(msg interface{}, tp Evt, opts []SockOpt) {
	if msg == nil || server == nil {
		return
	}
	so := sockOpts(opts)
	if so.room == "" {
		so.room = "bcast"
	}
	log := logrus.WithField("msg", msg)
	d, err := json.Marshal(msg)
	if err != nil {
		//todo how does one error in socketio
		log.WithError(err).Error("could not marshal data")
		server.BroadcastToRoom(so.namespace, so.room, so.evtPath(ErrorEvt), "error occured")
		return
	}
	server.BroadcastToRoom(so.namespace, so.room, so.evtPath(tp), string(d))
}

var wsAddr string

func init() {
	server = socketio.NewServer(&engineio.Options{
		Transports: []transport.Transport{
			&polling.Transport{
				CheckOrigin: allowOriginFunc,
			},
			&websocket.Transport{
				CheckOrigin: allowOriginFunc,
			},
		},
	})
	server.OnConnect("/", func(s socketio.Conn) error {
		s.SetContext("")
		s.Join("bcast")
		return nil
	})
	go func() {
		go server.Serve()
		defer server.Close()
		http.Handle("/socket.io/", server)
		l, err := net.Listen("tcp", ":0")
		if err != nil {
			logrus.Fatal(err)
		}
		wsAddr = l.Addr().String()
		logrus.Fatal(http.Serve(l, nil))
	}()
}

func WSAddr() string {
	return wsAddr
}

package input

import (
	"fmt"
	"math"
	"strings"

	"github.com/guark/guark/app"
	"gitlab.com/luberryscc/led-spy-designer/lib/funcs/bits"
	proto "gitlab.com/luberryscc/led-spy-protos/go"
)

func Reset(app *app.App) {
	app.ExecJS(`{const svg=document.querySelector(".injected-svg"); svg && svg.querySelectorAll(".active").forEach((e)=>e.classList.remove("active"))}`)
	app.ExecJS(`{const knob = document.getElementById("knob0"); knob&&knob.setAttribute("transform", "translate(0,0)")}`)
}

type State struct {
	Buttons    []bool `json:"buttons"`
	Profile    int    `json:"profile"`
	DPAD       bool   `json:"dpadSwitch"`
	LSX        int8   `json:"lSX"`
	LSY        int8   `json:"lSY"`
	app        *app.App
	numButtons int
	analog     bool
}

func (s *State) setButtons() {
	// TODO: limit this to the number of buttons a controller has
	for idx := 0; idx < s.numButtons && idx < len(s.Buttons); idx++ {
		val := s.Buttons[idx]
		action := add
		if !val {
			action = remove
		}
		idQuery := fmt.Sprintf("btn%d", idx)
		if s.analog && knobButton(idx) {
			s.manageClass(idQuery, []string{".outline", ".arrow"}, action)
			continue
		}
		s.manageClass(idQuery, []string{
			".ring",
			".plunger",
			".plungerdown",
			".plunger-text",
		}, action)

	}

}

type classAction string

const (
	add    classAction = "add"
	remove classAction = "remove"
)

func (s *State) manageClass(idQuery string, classses []string, action classAction) {
	var b strings.Builder
	b.WriteString(
		`{
		const btn=document.getElementById("`)
	b.WriteString(idQuery)
	b.WriteString(`");
		if (btn) {;
		`)
	for _, class := range classses {
		b.WriteString(`btn.querySelector("`)
		b.WriteString(class)
		b.WriteString(`").classList.`)
		b.WriteString(string(action))
		b.WriteString(`("active");
		`)
	}
	b.WriteString("}\n}")
	s.app.ExecJS(b.String())
}
func (s *State) Exec() {
	s.setButtons()
	s.knobTransform()
}
func stickValue(val int8) float32 {
	if val == 0 {
		return 0.0
	}
	return float32(val) / 10
}
func (s *State) knobTransform() {
	if !s.analog {
		return
	}
	s.app.ExecJS(fmt.Sprintf(`{const knob = document.getElementById("knob0"); knob&&knob.setAttribute("transform", "translate(%f,%f)")}`, stickValue(s.LSX), stickValue(s.LSY)))
}

const NumSpyButtons = 29

func Default() *State {
	return &State{Buttons: make([]bool, NumSpyButtons)}
}

type DecodeOption func(d *Decoder)

func New(opts ...DecodeOption) *Decoder {
	d := &Decoder{}
	for _, opt := range opts {
		opt(d)
	}
	return d
}

func (d *Decoder) isHB() bool {
	return d.controllerType == proto.Config_HIT_BOX
}

func WithApp(app *app.App) DecodeOption {
	return func(d *Decoder) {
		d.app = app
	}
}

func WithNumButtons(nb int) DecodeOption {
	return func(d *Decoder) {
		d.numButtons = nb
	}
}
func WithControllerType(tp proto.Config_ControllerType) DecodeOption {
	return func(d *Decoder) {
		d.controllerType = tp
	}
}
func WithControllerVariant(v proto.Config_ControllerVariant) DecodeOption {
	return func(d *Decoder) {
		d.controllerVar = v
	}
}

type Decoder struct {
	controllerType proto.Config_ControllerType
	controllerVar  proto.Config_ControllerVariant
	inputMappings  map[int]int
	app            *app.App
	numButtons     int
}

func supportsAnalog(v proto.Config_ControllerVariant) bool {
	return v == proto.Config_FIGHTSTICK || v == proto.Config_FIGHTSTICK_EXD
}
func knobButton(idx int) bool {
	return idx >= 8 && idx <= 11
}
func (d *Decoder) setAnalogFromButtonState(axes []int8, idx int, state bool) {
	if !state {
		return
	}
	if len(axes) < 2 {
		return
	}
	if !supportsAnalog(d.controllerVar) ||
		!knobButton(idx) {
		return
	}

	axis := 0
	if idx > 9 {
		axis = 1
	}
	switch idx {
	// up and left should be negative
	case 8, 10:
		axes[axis] = math.MinInt8 + 1
		return
	case 9, 11:
		axes[axis] = math.MaxInt8
		return
	}

}

func (d *Decoder) setButton(btns []bool, axes []int8, idx int, state bool) {
	if d == nil || d.inputMappings == nil {
		btns[idx] = state
		d.setAnalogFromButtonState(axes, idx, state)
		return
	}

	mp, ok := d.inputMappings[idx]
	if !ok {
		btns[idx] = state
		d.setAnalogFromButtonState(axes, idx, state)
		return
	}
	btns[mp] = state
	d.setAnalogFromButtonState(axes, mp, state)
}

func (d *Decoder) Decode(data []byte) (*State, error) {
	l := len(data)
	if l < 5 {
		return nil, fmt.Errorf("data too short. data: %q  should have len >= 5: got %d", string(data), l)
	}
	i := 0
	j := 0
	btns := make([]bool, NumSpyButtons)
	axes := make([]int8, 2)
	for _, b := range data[:l-1] {
		for i := 0; j < NumSpyButtons && i < 8; i++ {
			d.setButton(btns, axes, j, bits.BitRead(uint64(b), i))
			j++
		}
	}
	// TODO: support analog add another set of bits
	dpad := bits.BitRead(uint64(data[l-2]), i)
	return &State{
		Buttons:    btns,
		Profile:    int(data[l-1]),
		DPAD:       dpad,
		LSY:        axes[0],
		LSX:        axes[1],
		app:        d.app,
		numButtons: d.numButtons,
		analog:     supportsAnalog(d.controllerVar),
	}, nil
}

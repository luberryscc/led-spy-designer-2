package main

import (
	"flag"
	"html/template"
	"io"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"

	"github.com/google/uuid"
	"github.com/guark/guark/cmd/guark/builders"
	"github.com/sirupsen/logrus"
	"gopkg.in/yaml.v3"
)

var dest string

func init() {
	flag.StringVar(&dest, "dest", "dist", "place to put generated file")
	flag.Parse()
}
func main() {

	type b struct {
		Publisher, Description string
	}
	bundler := &b{}

	d, err := ioutil.ReadFile("guark-bundle.yaml")
	if err != nil {
		logrus.WithError(err).Fatal("could not read bundler config")
	}

	if err := yaml.Unmarshal(d, bundler); err != nil {
		logrus.WithError(err).Fatal("could not unmarshal bundler config")
	}
	ai := &builders.GuarkConfig{}
	d, err = ioutil.ReadFile("guark.yaml")
	if err != nil {
		logrus.WithError(err).Fatal("could not read bundler config")
	}
	if err := yaml.Unmarshal(d, ai); err != nil {
		logrus.WithError(err).Fatal("could not unmarshal bundler config")
	}

	cfgData, err := ioutil.ReadFile("app.tpl.iss")
	if err != nil {
		logrus.WithError(err).Fatal("could not read iss template")
	}

	f, err := os.Create(filepath.Join("app.iss"))
	if err != nil {
		logrus.WithError(err).Fatal("could read iss template")
	}
	defer f.Close()

	data := map[string]interface{}{
		"ID":          ai.ID,
		"Ver":         strings.TrimPrefix(ai.Version, "v"),
		"Name":        ai.Name,
		"Version":     ai.Version,
		"Publisher":   bundler.Publisher,
		"Dest":        dest,
		"Description": bundler.Description,
	}

	if err = execTemplate(f, string(cfgData), data); err != nil {
		logrus.WithError(err).Fatal("could not execute template")
	}

}
func execTemplate(w io.Writer, templ string, data map[string]interface{}) error {

	funcs := template.FuncMap{
		"guid": func() string {
			return uuid.New().String()
		},
	}

	tmpl, err := template.New("BundlerTemplate").Funcs(funcs).Parse(templ)
	if err != nil {
		return err
	}

	return tmpl.Execute(w, data)
}
